const express = require('express');
const router = express.Router();
const Device = require('../models/device');
const DeviceConfig = require('../models/device_config');
const BasicConfig = require('../models/basic_config');
const config = require('../config/database');


// add device config
router.post('/addDevice', (req, res, next) => {

    let newDevice = new Device({
        key: req.body.key,
        category: req.body.category,
        address: req.body.address
    });

    Device.addDevice(newDevice, (err) => {
        if(err){
            res.json({success: false, msg:'Failed to add new device: ' + err.message});
        } else {
            res.json({success: true, msg:'Device ' + newDevice + ' added'});
        }
    });

});

// remove device config
router.post('/delDevices', (req, res, next) => {

    Device.removeDevices((err) => {
        if(err){
            res.json({success: false, msg:'Failed to remove devices: ' + err.message});
        } else {
            res.json({success: true, msg:'Device removed successfully'});
        }
    });

});

// get one device
router.post('/getDevice', (req, res, next) => {

    const key = req.body.key;
    Device.getDeviceByKey(key, (err, dev)=>{

        if(err) {
            return res.json({success: false, msg: 'Device not found: ' + err.message})
        }
        if(!dev){
            return res.json({success: false, msg: 'Device not found'})
        }


        res.json({
            success: true,
            device: {
                key: dev.key,
                category: dev.category,
                address: dev.address
            }
        });

    });
});


// get devices
router.post('/getDevices', (req, res, next) => {

    const keys = req.body.keys;

    Device.getDevicesByKey(keys, (err, dev)=>{

        if(err) {
            return res.json({success: false, msg: 'Get devices error: ' + err.message})
        }
        if(!dev){
            return res.json({success: false, msg: 'Devices not found'})
        }

        var devices= new Array();

        dev.forEach(function(device) {
            devices.push(
                {
                    key: device.key,
                    address: device.address,
                    category: device.category
                }
            );
        });

        res.json({
            success: true,
            devices: devices

        });

    });
});


// set basic config
router.post('/addConfig', (req, res, next) => {

    let config = new BasicConfig({
        key: req.body.key,
        value: req.body.value
    });

    BasicConfig.setConfig(config, (err) => {
        if(err){
            res.json({success: false, msg:'Failed to set new config: ' + err.message});
        } else {
            res.json({success: true, msg:'Config ' + config + ' added'});
        }
    });

});

// get basic config
router.post('/getConfig', (req, res, next) => {

    const key = req.body.key;
    BasicConfig.getConfig(key, (err, conf)=>{

        if(err) {
            return res.json({success: false, msg: 'Get config error: ' + err.message})
        }
        if(!conf){
            return res.json({success: false, msg: 'Config not found'})
        }

        res.json({
            success: true,
            config: {
                key: conf.key,
                value: conf.value
            }
        });

    });

});


// remove basic config
router.post('/delConfig', (req, res, next) => {

    BasicConfig.removeConfigs((err) => {
        if(err){
            res.json({success: false, msg:'Failed to remove configs: ' + err.message});
        } else {
            res.json({success: true, msg:'Configs removed successfully'});
        }
    });

});

// set basic config
router.post('/addDeviceConfig', (req, res, next) => {

    let config = new DeviceConfig({
        key: req.body.key,
        address: req.body.address,
        category: req.body.category,
        parameter: req.body.parameter,
        value: req.body.value
    });

    DeviceConfig.addConfig(config, (err) => {
        if(err){
            res.json({success: false, msg:'Failed to set new device config: ' + err.message});
        } else {
            res.json({success: true, msg:'DeviceConfig ' + config + ' added'});
        }
    });

});

// get devices config
router.post('/getDevicesConfig', (req, res, next) => {

    const key = req.body.key;

    DeviceConfig.getConfigByKey(key, (err, conf)=>{

        if(err) {
            return res.json({success: false, msg: 'Get devices config error: ' + err.message})
        }
        if(!conf){
            return res.json({success: false, msg: 'DeviceConfigs not found'})
        }

        var configs = new Array();

        conf.forEach(function(config) {
            configs.push(
                {
                    key: config.key,
                    address: config.address,
                    category: config.category,
                    parameter: config.parameter,
                    value: config.value
                }
            );
        });

        res.json({
            success: true,
            configs: configs

        });

    });
});

// remove basic config
router.post('/delDevicesConfig', (req, res, next) => {

    DeviceConfig.removeConfigs((err) => {
        if(err){
            res.json({success: false, msg:'Failed to remove DeviceConfigs: ' + err.message});
        } else {
            res.json({success: true, msg:'DeviceConfigs removed successfully'});
        }
    });

});

module.exports = router;
