const express = require('express'); 
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require('../config/database');


//
// // Register
// router.post('/register', (req, res, next) => {
//     //res.send('REGISTER');
//     let newUser = new User({
//         name: req.body.name,
//         email: req.body.email,
//         username: req.body.username,
//         password: req.body.password
//     });
//
//     User.addUser(newUser, (err, user) => {
//         if(err){
//             res.json({success: false, msg:'Failed to register user'});
//         } else {
//             res.json({success: true, msg:'User registered'});
//         }
//     });
// });

// Authenticate
router.post('/authenticate', (req, res, next) => {
    //res.send('AUTHENTICATE');
    const usrname = req.body.username;
    const psswd = req.body.password;
    const neverExp = req.body.rememberme;

    console.log('>>>> auth: username: ' + usrname);
    User.validatePassword(usrname, psswd, (err, success, response) => {
        if(err) throw err;

        var expiresVal = neverExp ? 90000000 : 3600;

        if (success) {
            const token = jwt.sign({name: usrname, username: usrname, password: psswd, email:'no@test.com'}, config.secret, {
               expiresIn: expiresVal //1week
            });

            res.json({
                success: true,
                token: 'JWT ' + token,
                user: usrname,
                expiresIn: expiresVal
            })

        } else {
            return res.json({success: false, msg: 'Authentication failed: ' + response})
        }
    });


});


module.exports = router;
