const express = require('express');
const router = express.Router();
const ActionWidgetsGroup = require('../models/action_widgets_group');
const ActionWidget = require('../models/action_widget');
const WidgetAction = require('../models/widget_action');
const Device = require('../models/device');
const DeviceConfig = require('../models/device_config');
const BasicConfig = require('../models/basic_config');


//insertActionWidgetsGroups
router.post('/actionWidgetsGroups', (req, res, next) => {
    if (req.body && req.body.actionWidgetsGroups != undefined) {

        let groups = [];

        for (var gr of req.body.actionWidgetsGroups) {
            let group = new ActionWidgetsGroup({
                key: gr.key,
                name: gr.name
            });

            groups.push(group);
        }



        ActionWidgetsGroup.insertActionWidgetsGroups(groups, (err) => {
            if(err){
                res.json({success: false, msg:'Failed to insert Action Widgets Groups: ' + err.message});
            } else {
                // res.json({success: true, msg:'Config ' + config + ' added'});
                res.json({success: true, msg: 'Action Widgets Groups inserted'});
            }
        });

        //return res.json({success: true, msg: 'Action Widgets Group upserted'});
    } else {
        res.json({success: false, msg:'Invalid parameters for inserting Action Widget Groups'});
        //return res.json({success: false, msg: 'Invalid data received'});
    }
});


//addUpdateActionWidgets
router.post('/actionWidgets', (req, res, next) => {
  
    if (req.body && req.body.actionWidgets != undefined) {

        let widgets = [];

        for (var wid of req.body.actionWidgets) {

            let widget = new ActionWidget({
                key: wid.key,
                groupKey: wid.groupKey,
                type: wid.type,
                active: wid.active
            });

            if (wid.details != undefined) {
                widget.details = wid.details;
            }
            if (wid.name != undefined) {
                widget.name = wid.name;
            }

            widgets.push(widget);

        }

        ActionWidget.insertActionWidgets(widgets, (err) => {
            console.log('>>> err: ', err);
            if(err){
                res.json({success: false, msg:'Failed to insert Action Widgets: ' + err.message});
            } else {
                // res.json({success: true, msg:'Config ' + config + ' added'});
                res.json({success: true, msg: 'Action Widgets upserted'});
            }
        });

        //return res.json({success: true, msg: 'Action Widgets Group upserted'});
    } else {
        res.json({success: false, msg:'Invalid parameters for upserting Action Widget'});
        //return res.json({success: false, msg: 'Invalid data received'});
    }
});


//upsertWidgetAction
router.post('/widgetActions', (req, res, next) => {
    if (req.body && req.body.widgetActions != undefined) {

        let actions = [];

        for (var act of req.body.widgetActions) {
            let action = new WidgetAction({
                key: act.key,
                widgetKey: act.widgetKey,
                widgetEvent: act.widgetEvent,
                devCategory: act.devCategory,
                devAddress: act.devAddress
            });

            if (act.devCommand != undefined) {
                action.devCommand = act.devCommand;
            }
            if (act.devCommandValue != undefined) {
                action.devCommandValue = act.devCommandValue;
            }

            actions.push(action);
        }


        WidgetAction.insertWidgetActions(actions, (err) => {
            console.log('>>> err: ', err);
            if(err){
                res.json({success: false, msg:'Failed to insert Widget Actions: ' + err.message});
            } else {
                // res.json({success: true, msg:'Config ' + config + ' added'});
                res.json({success: true, msg: 'Widget Actions inserted'});
            }
        });

        //return res.json({success: true, msg: 'Action Widgets Group upserted'});
    } else {
        res.json({success: false, msg:'Invalid parameters for upserting Widget Action'});
        //return res.json({success: false, msg: 'Invalid data received'});
    }
});


// insert all devices
router.post('/devices', (req, res, next) => {

    if (req.body && req.body.devices != undefined) {

        let devices = [];

        for (var dev of req.body.devices) {
            let newDevice = new Device({
                key: dev.key,
                category: dev.category,
                address: dev.address
            });

            devices.push(newDevice);
        }


        Device.insertAllDevices(devices, (err) => {
            console.log('>>> err: ', err);
            if(err){
                res.json({success: false, msg:'Failed to insert Devices: ' + err.message});
            } else {
                // res.json({success: true, msg:'Config ' + config + ' added'});
                res.json({success: true, msg: 'Devices inserted'});
            }
        });

        //return res.json({success: true, msg: 'Action Widgets Group upserted'});
    } else {
        res.json({success: false, msg:'Invalid parameters for inserting Devices'});
        //return res.json({success: false, msg: 'Invalid data received'});
    }

});


// insert all basic config
router.post('/basicConfigs', (req, res, next) => {

    if (req.body && req.body.basicConfigs != undefined) {

        let configs = [];

        for (var conf of req.body.basicConfigs) {
            let config = new BasicConfig({
                key: conf.key,
                value: conf.value
            });

            configs.push(config);
        }


        BasicConfig.insertAllBasicConfigs(configs, (err) => {
            console.log('>>> err: ', err);
            if(err){
                res.json({success: false, msg:'Failed to insert Basic Configs: ' + err.message});
            } else {
                // res.json({success: true, msg:'Config ' + config + ' added'});
                res.json({success: true, msg: 'Basic Configs inserted'});
            }
        });

        //return res.json({success: true, msg: 'Action Widgets Group upserted'});
    } else {
        res.json({success: false, msg:'Invalid parameters for inserting Basic Configs'});
        //return res.json({success: false, msg: 'Invalid data received'});
    }

});


// insert all device config
router.post('/deviceConfigs', (req, res, next) => {

    if (req.body && req.body.deviceConfigs != undefined) {

        let configs = [];

        for (var conf of req.body.deviceConfigs) {
            let config = new DeviceConfig({
                key: conf.key,
                address: conf.address,
                category: conf.category,
                parameter: conf.parameter,
                value: conf.value
            });

            configs.push(config);
        }


        DeviceConfig.insertAllDeviceConfigs(configs, (err) => {
            console.log('>>> err: ', err);
            if(err){
                res.json({success: false, msg:'Failed to insert Device Configs: ' + err.message});
            } else {
                // res.json({success: true, msg:'Config ' + config + ' added'});
                res.json({success: true, msg: 'Device Configs inserted'});
            }
        });

        //return res.json({success: true, msg: 'Action Widgets Group upserted'});
    } else {
        res.json({success: false, msg:'Invalid parameters for inserting Device Configs'});
        //return res.json({success: false, msg: 'Invalid data received'});
    }

});


module.exports = router;