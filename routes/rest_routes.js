const express = require('express');
const router = express.Router();
const RestClient = require('../rest/client');

//setDeviceParameter
router.post('/setDeviceParameter', (req, res, next) => {

    RestClient.setDeviceParameter(
        req.body.devCat,
        req.body.devAddr,
        req.body.param,
        req.body.val,
        res
    );

});

//getDeviceParameter
router.post('/getDeviceParameter', (req, res, next) => {

    RestClient.getDeviceParameter(
        req.body.devCat,
        req.body.devAddr,
        req.body.param,
        res
    );
});

//getJson
router.post('/getJson', (req, res, next) => {

    RestClient.getJson(
        req.body.jsonId,
        res
    );

});

//saveJson
router.post('/saveJson', (req, res, next) => {

    RestClient.saveJson(
        req.body.jsonId,
        req.body.jsonBody,
        res
    );

});

//setDeviceName
router.post('/setDeviceName', (req, res, next) => {

    RestClient.setDeviceName(
        req.body.devCat,
        req.body.devAddr,
        req.body.name,
        res
    );

});

//getDevicesList
router.post('/getDevicesList', (req, res, next) => {

    RestClient.getDevicesList(
        res
    );

});

//getFilesList
router.post('/getFilesList', (req, res, next) => {

    RestClient.getFilesList(
        res
    );

});

//uploadFirmware
router.post('/uploadFirmware', (req, res, next) => {

    RestClient.uploadFirmware(
        req.body.fileName,
        res
    );

});

//bootDevice
router.post('/bootDevice', (req, res, next) => {

    RestClient.bootDevice(
        req.body.uid,
        res
    );

});

//initDevices
router.post('/initDevices', (req, res, next) => {

    RestClient.initDevices(
        res
    );

});

//removeDevice
router.post('/removeDevice', (req, res, next) => {

    RestClient.removeDevice(
        req.body.uid,
        res
    );

});

//removeDeviceAdvance
router.post('/removeDeviceAdvance', (req, res, next) => {

    RestClient.removeDeviceAdvance(
        req.body.cat,
        req.body.addrStart,
        req.body.addrStop,
        res
    );

});

//getDeviceDetails
router.post('/getDeviceDetails', (req, res, next) => {

    RestClient.getDeviceDetails(
        req.body.devCat,
        req.body.devAddr,
        res
    );

});

//getDeviceParameters
router.post('/getDeviceParameters', (req, res, next) => {

    RestClient.getDeviceParameters(
        req.body.devCat,
        req.body.devAddr,
        res
    );

});

//getCategories
router.post('/getCategories', (req, res, next) => {

    RestClient.getCategories(
        res
    );

});

//sendMessage
router.post('/sendMessage', (req, res, next) => {

    RestClient.sendMessage(
        req.body.number,
        req.body.message,
        res
    );

});

//armSystem
router.post('/armSystem', (req, res, next) => {

    RestClient.armSystem(
        req.body.armDisarm,
        req.body.hash,
        res
    );

});

//getArmSystemStatus
router.post('/getArmSystemStatus', (req, res, next) => {

    RestClient.getArmSystemStatus(
        res
    );

});

module.exports = router;
