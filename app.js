const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');
const ClientLog = require('./models/client_logger');


mongoose.connect(config.database);

mongoose.connection.on('connected', ()=>{
    console.log('Connected to DB' + config.database);
});

mongoose.connection.on('error', (err)=>{
    console.log('Database error: ' + err);
});

const app = express();

const users = require('./routes/users');
const rest_routes = require('./routes/rest_routes');
const config_routes = require('./routes/config_routes');
const tools_routes = require('./routes/tools_routes');
const backup_routes = require('./routes/backup_routes');
const restore_routes = require('./routes/restore_routes');

const port = 3000;
//
app.use(cors());
//
////Set static folders
app.use(express.static(path.join(__dirname, 'public')));
//
//Body parser middleware
app.use(bodyParser.json());
//
//
////Passport middleware
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);
//
app.use('/users', users);
app.use('/rest', rest_routes);
app.use('/config', config_routes);
app.use('/tools', tools_routes);
app.use('/backup', backup_routes);
app.use('/restore', restore_routes);
//
app.get('/', (req, res)=>{
    res.send('Invalid endpoint');
});
//
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/index.html'));
})

app.listen(port, () => {
    console.log('Server started on port: ' + port);
});


setInterval(function() {  
   console.log('>>> Removing old client logs');

   ClientLog.dropOldLogs((err, clientLogs)=>{

        if (err) {
        	console.log('>>> ERROR during drop old logs: ' + err);
        } 

    });
 
}, 12 * 3600000);