import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'logsFilter'
})
export class LogsFilterPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
    	return value.slice(value.indexOf(']') + 1);
    }
  }

}
