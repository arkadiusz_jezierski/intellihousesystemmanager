export class ConfigConsts {

  public static LIVINGROOM_LIGHTS_SET = 'livingroomlightsset';
  public static LIVINGROOM_LIGHTS_SET_1 = 'livingroomlightsset1';
  public static LIVINGROOM_LIGHTS_SET_2 = 'livingroomlightsset2';
  public static LIVINGROOM_LIGHTS_SET_3 = 'livingroomlightsset3';
  public static LIVINGROOM_LIGHTS_SET_4 = 'livingroomlightsset4';
  public static LIVINGROOM_LIGHTS_SET_MANUAL = 'livingroomlightsmanual';

  public static LIVINGROOM_WALL_EFFECT_SET_1 = 'livingroomwalleffectset1';
  public static LIVINGROOM_WALL_EFFECT_SET_2 = 'livingroomwalleffectset2';
  public static LIVINGROOM_WALL_EFFECT_SET_3 = 'livingroomwalleffectset3';
  public static LIVINGROOM_WALL_EFFECT_SET_4 = 'livingroomwalleffectset4';

  public static LIVINGROOM_CEILING_EFFECT_SET_1 = 'livingroomceilingeffectset1';
  public static LIVINGROOM_CEILING_EFFECT_SET_2 = 'livingroomceilingeffectset2';
  public static LIVINGROOM_CEILING_EFFECT_SET_3 = 'livingroomceilingeffectset3';



}
