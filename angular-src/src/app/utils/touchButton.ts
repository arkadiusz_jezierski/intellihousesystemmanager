import {
  Directive,
  Output,
  EventEmitter,
  HostListener
} from '@angular/core';

@Directive({ selector: '[touch-button]' })
export class TouchButton {

  buttonTouched : boolean;

  @Output() onTouch: EventEmitter<any> = new EventEmitter();

  @HostListener('touchstart', ['$event'])
  onTouchStart(event) {
    console.log('>>>> on omuse touch');
    this.buttonTouched = true;
    this.onTouch.emit(event);
  }

  @HostListener('mousedown', ['$event'])
  onMouseDown(event) {
      console.log('>>>> on omuse down: ' + this.buttonTouched);
    if (this.buttonTouched === undefined || this.buttonTouched === false) {  
      this.onTouch.emit(event);
    }
  }

  @HostListener('mouseup')
  onMouseUp() {
    this.buttonTouched = false;
    console.log('>>> onmouseup: ' + this.buttonTouched);
  }

  @HostListener('touchend')
  onTouchEnd() {console.log('>>> ontouchend: ' + this.buttonTouched);}


}
