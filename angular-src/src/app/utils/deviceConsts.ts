export class DeviceConsts {

  public static KITCHEN_MAIN = 'kuchnia_sufit';
  public static KITCHEN_SIDE = 'kuchnia_boczne';
  public static KITCHEN_FURN_LEFT = 'kuchnia_szafki_lewe';
  public static KITCHEN_FURN_RIGHT = 'kuchnia_szafki_prawe';
  public static LIVINGROOM_LIGHTS_RIM_01 = 'salon_zew_01';
  public static LIVINGROOM_LIGHTS_RIM_02 = 'salon_zew_02';
  public static LIVINGROOM_LIGHTS_RIM_03 = 'salon_zew_03';
  public static LIVINGROOM_LIGHTS_RIM_04 = 'salon_zew_04';
  public static LIVINGROOM_LIGHTS_RIM_05 = 'salon_zew_05';
  public static LIVINGROOM_LIGHTS_RIM_06 = 'salon_zew_06';
  public static LIVINGROOM_LIGHTS_RIM_07 = 'salon_zew_07';
  public static LIVINGROOM_LIGHTS_RIM_08 = 'salon_zew_08';
  public static LIVINGROOM_LIGHTS_RIM_09 = 'salon_zew_09';
  public static LIVINGROOM_LIGHTS_RIM_10 = 'salon_zew_10';
  public static LIVINGROOM_LIGHTS_RIM_11 = 'salon_zew_11';
  public static LIVINGROOM_LIGHTS_RIM_12 = 'salon_zew_12';
  public static LIVINGROOM_LIGHTS_RIM_13 = 'salon_zew_13';
  public static LIVINGROOM_LIGHTS_RIM_14 = 'salon_zew_14';
  public static LIVINGROOM_LIGHTS_MIDDLE_01 = 'salon_wew_01';
  public static LIVINGROOM_LIGHTS_MIDDLE_02 = 'salon_wew_02';
  public static LIVINGROOM_LIGHTS_MIDDLE_03 = 'salon_wew_03';
  public static LIVINGROOM_LIGHTS_MIDDLE_04 = 'salon_wew_04';
  public static LIVINGROOM_LIGHTS_MIDDLE_05 = 'salon_wew_05';
  public static LIVINGROOM_LIGHTS_MIDDLE_06 = 'salon_wew_06';
  public static LIVINGROOM_LIGHTS_MIDDLE_07 = 'salon_wew_07';
  public static LIVINGROOM_LIGHTS_MIDDLE_08 = 'salon_wew_08';

  public static LIVINGROOM_WALL_EFFECT_TOP = 'salon_okno_gora';
  public static LIVINGROOM_WALL_EFFECT_MIDDLE = 'salon_okno_srodek';
  public static LIVINGROOM_WALL_EFFECT_BOTTOM = 'salon_okno_dol';
  public static LIVINGROOM_CEILING_EFFECT_OUTSIDE_LONG = 'salon_listwa_zew_dluga';
  public static LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT = 'salon_listwa_zew_krotka';
  public static LIVINGROOM_CEILING_EFFECT_INSIDE_LONG = 'salon_listwa_wew_dluga';
  public static LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT = 'salon_listwa_wew_krotka';

  public static KITCHEN_SOCKET_01 = 'kuchnia_gniazdko_01';
  public static KITCHEN_SOCKET_02 = 'kuchnia_gniazdko_02';
  public static KITCHEN_SOCKET_03 = 'kuchnia_gniazdko_03';
  public static KITCHEN_SOCKET_04 = 'kuchnia_gniazdko_04';
  public static KITCHEN_SOCKET_05 = 'kuchnia_gniazdko_05';
  public static KITCHEN_SOCKET_06 = 'kuchnia_gniazdko_06';
  public static LIVINGROOM_SOCKET_01 = 'salon_gniazdko_01';


  public static PARAM_STATE = 'state';
  public static PARAM_SWITCH = 'switch';
  public static PARAM_DUTY_CYCLE = 'dutyCycle';
  public static PARAM_DUTY_CYCLE_UP_ALL = 'dutyUpAll';
  public static PARAM_DUTY_CYCLE_DOWN_ALL = 'dutyDownAll';
  public static PARAM_DUTY_CYCLE_ALL = 'dutyCycleAllTheSame';
  public static PARAM_RED = 'red';
  public static PARAM_GREEN = 'green';
  public static PARAM_BLUE = 'blue';
  public static PARAM_SPEED = 'speed';
  public static PARAM_MODE = 'mode';
  public static PARAM_RGB = 'rgb';

  public static COMM_OFF = 'off';

  public static RGB_MODE_OFF = 0;
  public static RGB_MODE_AUTO1 = 1;
  public static RGB_MODE_AUTO2 = 2;
  public static RGB_MODE_RND = 3;
  public static RGB_MODE_MANUAL = 4;
  public static RGB_MODE_RND_ALL = 5;

  public static DEV_CATEGORY_BISTABLE_SENSOR = '2';
  public static DEV_CATEGORY_BISTABLE_SWITCH = '3';
  public static DEV_CATEGORY_PWM = '5';
  public static DEV_CATEGORY_MOTION_DETECTOR = '6';
  public static DEV_CATEGORY_RGB = '7';


}
