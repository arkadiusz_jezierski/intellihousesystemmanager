export class StorageFactory {
  public inMemoryStorage: Object = {};
  public storage;

  constructor(storage) {
    this.storage = storage;
  }


  isSupported() : Boolean {
    try {
      const key = "__some_random_key_you_are_not_going_to_use__";
      this.storage.setItem(key, key);
      var tmp = this.storage.getItem(key);
      if (tmp != key) {
        return false;
      }
      this.storage.removeItem(key);
      return true;
    } catch (e) {
      return false;
    }
  }

  getItem(key) {
    if (this.isSupported()) {
      return this.storage.getItem(key);
    }
    return this.inMemoryStorage[key] || null;
  }

  setItem(key, value) {
    if (this.isSupported()) {
      this.storage.setItem(key, value);
    } else {
      this.inMemoryStorage[key] = value;
    }
  }

  removeItem(key) {
    if (this.isSupported()) {
      this.storage.removeItem(key);
    } else {
      delete this.inMemoryStorage[key];
    }
  }

  clear() {
    if (this.isSupported()) {
      this.storage.clear();
    } else {
      this.inMemoryStorage = {};
    }
  }

  key(n) {
    if (this.isSupported()) {
      return this.storage.key(n);
    } else {
      return Object.keys(this.inMemoryStorage)[n] || null;
    }
  }

}
