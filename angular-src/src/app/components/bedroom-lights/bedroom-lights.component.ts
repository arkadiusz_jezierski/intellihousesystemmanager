import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services/rest.service';
import { ConfigService } from '../../services/config.service';
import { DeviceManagerService } from '../../services/device-manager.service';
import { Device } from '../../models/device';
import { DeviceConsts } from '../../utils/deviceConsts';
import { ConfigConsts } from '../../utils/configConsts';
import { ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {LightAdjustComponent} from "../living-room-lights/living-room-lights.component";


@Component({
  selector: 'bedrrom-lights',
  templateUrl: './bedroom-lights.component.html',
  styleUrls: ['./bedroom-lights.component.css']
})
export class BedroomLightsComponent implements OnInit {

  devicesMap: Object;
  popupDevKey: string;
  bsModalRef: BsModalRef;

  constructor(
    private restService:RestService,
    private deviceManager: DeviceManagerService,
    private configService: ConfigService,
    private route: ActivatedRoute,
    private modalService: BsModalService
  ) { }

  ngOnInit() {

  }

  getStyle(id) :number{
    if (this.devicesMap && this.devicesMap[id]) {
      return this.devicesMap[id].dutyCycle / 255;
    } else {
      return 1;
    }

  }

  switch(event) :void {
    var id = event['target']['attributes']['id'].value;

    var newDutyCycle;
    if (this.devicesMap[id].dutyCycle > 0) {
      newDutyCycle = 0;
    } else {
      newDutyCycle = 255;
    }

    this.devicesMap[id].dutyCycle = newDutyCycle;
    this.setDeviceParameter(this.devicesMap[id].device, DeviceConsts.PARAM_DUTY_CYCLE, newDutyCycle);
  }

  showPopup(event) {
    this.popupDevKey = event['target'].attributes['id'].value;
    this.popupDutyCycle = this.devicesMap[this.popupDevKey].dutyCycle;

    this.bsModalRef = this.modalService.show(LightAdjustComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
    this.bsModalRef.content.parentController = this;
  }

  onIncreaseDutyCycle() :void {
    this.popupDutyCycle += 5;
    if (this.popupDutyCycle > 255) {
      this.popupDutyCycle = 255;
    }
    this.setDeviceParameter(this.devicesMap[this.popupDevKey].device, DeviceConsts.PARAM_DUTY_CYCLE, this.popupDutyCycle);
  }

  onDecreaseDutyCycle() :void {
    if (this.popupDutyCycle > 4) {
      this.popupDutyCycle -= 5;
    } else {
      this.popupDutyCycle = 0;
    }
    this.setDeviceParameter(this.devicesMap[this.popupDevKey].device, DeviceConsts.PARAM_DUTY_CYCLE, this.popupDutyCycle);
  }

  save(): void {
    if (this.lightSet != ConfigConsts.LIVINGROOM_LIGHTS_SET_MANUAL) {
      for (var devKey of this.currentDevices) {
        this.configService.addDeviceConfig(this.lightSet, this.devicesMap[devKey].device, DeviceConsts.PARAM_DUTY_CYCLE, this.devicesMap[devKey].dutyCycle).subscribe(data=>{
          console.log('Saving config ' + this.lightSet + ', device ' + this.devicesMap[devKey].device + ' duty cycle = ' + this.devicesMap[devKey].dutyCycle);
        });
      }

      this.saveSetName();
    }
  }

  cancel(): void {
    console.log('>>> CANCEL');

    for (var devKey of this.currentDevices) {
      this.setDeviceParameter(this.devicesMap[devKey].device, DeviceConsts.PARAM_DUTY_CYCLE, this.devicesMap[devKey].initDutyCycle);
    }

  }

  loadSetName() :void {
    this.configService.getBasicConfig(this.lightSet).subscribe(data => {
      if (data.success) {
        this.lightSetName = data.config.value;
      } else {
        console.log('GetConfig error: ', data.msg);
      }

    });
  }


  saveSetName() :void {
    this.configService.addBasicConfig(this.lightSet, this.lightSetName).subscribe(data=>{
      if (data.success) {
        console.log('LighSetName saved: ' , this.lightSetName);
      } else {
        console.log('Saving lightSetName error: ', data.msg);
      }
    });
  }

  loadDevices(): void {
    console.log('Loading devices config');

    this.configService.getDevices(this.currentDevices).subscribe(data => {
      console.log('data: ', data);
      if (data.success) {
        console.log('GetDevices success, devices list: ', data.devices);
        this.devicesMap = new Object();

        for (var dev of data.devices) {
          console.log('dev:' ,dev);
          this.devicesMap[dev.key] = new Object();
          this.devicesMap[dev.key].device = this.deviceManager.getDevice(dev.category, dev.address);
          this.devicesMap[dev.key].dutyCycle = 0;
          this.devicesMap[dev.key].initDutyCycle = 0;
        }

        if (this.lightSet == ConfigConsts.LIVINGROOM_LIGHTS_SET_MANUAL) {
          this.loadDevicesParameters();
        } else {
          this.loadDevicesParametersFromConfig(this.lightSet);
        }

        console.log('>>>> devices map: ', this.devicesMap);

      } else {
        console.log('GetDevices error: ', data.msg);
      }


    });
  }

  loadDevicesParameters(): void {
    console.log('Loading devices parameters');
    for (var devKey of this.currentDevices) {

      this.loadDeviceParameter(devKey, DeviceConsts.PARAM_DUTY_CYCLE, (key, val) => {
        this.devicesMap[key].dutyCycle = val;
        this.devicesMap[key].initDutyCycle = val;
      });

      console.log('Device ' + devKey + ' has duty cycle = ' + this.devicesMap[devKey].dutyCycle);
    }
  }

  loadDevicesParametersFromConfig(set :string): void {
    console.log('Loading devices parameters from config: ' + set);
    for (var devKey of this.currentDevices) {
      this.loadDeviceParameter(devKey, DeviceConsts.PARAM_DUTY_CYCLE, (key, val) => {
        this.devicesMap[key].initDutyCycle = val;
      });
      console.log('Device ' + devKey + ' has duty cycle = ' + this.devicesMap[devKey].dutyCycle);
    }

    this.configService.getDevicesConfigs(this.lightSet).subscribe(data => {
      console.log('data: ', data);
      if (data.success) {
        console.log('config: ', data.configs);
        for (var config of data.configs) {

          var key = this.getDeviceKey(config.address, config.category);
          this.devicesMap[key].dutyCycle = config.value;

          console.log('Device ' + devKey + ' has duty cycle in config = ' + this.devicesMap[key].dutyCycle);
        }

        console.log('Setting devices parameter');
        for (var devKey of this.currentDevices) {
          this.setDeviceParameter(this.devicesMap[devKey].device, DeviceConsts.PARAM_DUTY_CYCLE, this.devicesMap[devKey].dutyCycle);
        }
      } else {
        console.log('GetDevicesConfigs error: ', data.msg);
      }


    });
  }

  getDeviceKey(address: string, category: string) :string{
    for (var devKey of this.currentDevices) {
      if (this.devicesMap[devKey].device.address == address && this.devicesMap[devKey].device.category == category) {
        return devKey;
      }
    }
  }

  loadDeviceParameter(deviceKey: string, param: string, callback: (key: string, val: number) => void) :void {
    this.restService.getDeviceParameter(this.devicesMap[deviceKey].device, param).subscribe(data => {
      if(data.errorCode){
        console.log('Loading device parameters failed: ' + data.result);
      } else {
        callback(deviceKey, data.value);
      }
    });
  }

  switch(event) :void {
    var id = event['target']['attributes']['id'].value;

    var newDutyCycle;
    if (this.devicesMap[id].dutyCycle > 0) {
      newDutyCycle = 0;
    } else {
      newDutyCycle = 255;
    }

    this.devicesMap[id].dutyCycle = newDutyCycle;
    this.setDeviceParameter(this.devicesMap[id].device, DeviceConsts.PARAM_DUTY_CYCLE, newDutyCycle);
  }

  getStyle(id) :number{
    if (this.devicesMap && this.devicesMap[id]) {
      return this.devicesMap[id].dutyCycle / 255;
    } else {
      return 1;
    }

  }


  setDeviceParameter(dev: Device, param: string, val: number) : void {
    this.restService.setDeviceParameter(dev, param, val).subscribe(data => {
      if(data.errorCode){
        console.log('Setting device parameter failed: ' + data.result);
      } else {
        console.log('Device parameter set successfully!');
      }
    });
  }

  ngOnInit() {
    console.log('>>> onInit');
    this.route.params.subscribe(params => {
      this.lightSet = params['id'];
      console.log('>>>> init set: ' + this.lightSet);
    });

    this.loadSetName();
    this.deviceManager.loadDevices(() => {this.loadDevices();});
    this.showName = (this.lightSet != ConfigConsts.LIVINGROOM_LIGHTS_SET_MANUAL);
  }

  showSetNameInput() {
    return this.lightSet != ConfigConsts.LIVINGROOM_LIGHTS_SET_MANUAL;
  }

  showPopup(event) {
    this.popupDevKey = event['target'].attributes['id'].value;
    this.popupDutyCycle = this.devicesMap[this.popupDevKey].dutyCycle;

    this.bsModalRef = this.modalService.show(LightAdjustComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
    this.bsModalRef.content.parentController = this;
  }

  onPopupValueChange() :void {
    console.log('DutyCycle in popup: ' + this.popupDutyCycle);
    this.setDeviceParameter(this.devicesMap[this.popupDevKey].device, DeviceConsts.PARAM_DUTY_CYCLE, this.popupDutyCycle);
  }

  onSavePopup() :void {
    this.devicesMap[this.popupDevKey].dutyCycle = this.popupDutyCycle;
  }

  onCancelPopup() :void {
    this.setDeviceParameter(this.devicesMap[this.popupDevKey].device, DeviceConsts.PARAM_DUTY_CYCLE, this.devicesMap[this.popupDevKey].dutyCycle);
  }

}


