import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceAdvanceControlPwmComponent } from './device-advance-control-pwm.component';

describe('DeviceAdvanceControlPwmComponent', () => {
  let component: DeviceAdvanceControlPwmComponent;
  let fixture: ComponentFixture<DeviceAdvanceControlPwmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceAdvanceControlPwmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceAdvanceControlPwmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
