import { Component, Input, OnInit, OnChanges, SimpleChange } from '@angular/core';
import { Options } from 'ng5-slider';
import { Device } from '../../models/device';
import { DeviceConsts } from '../../utils/deviceConsts';
import { RestService } from '../../services/rest.service';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-device-advance-control-pwm',
  templateUrl: './device-advance-control-pwm.component.html',
  styleUrls: ['./device-advance-control-pwm.component.css']
})
export class DeviceAdvanceControlPwmComponent implements OnInit, OnChanges {

  @Input()  device: Device;

  allDevicesSwitch : boolean;
  value : number;
  options: Options;

  constructor (
    private restService:RestService,
    private flashMessage:FlashMessagesService
  ) {}

  ngOnInit() {
    this.options = new Options();
    this.options.floor = 0;
    this.options.ceil = 255;
    this.allDevicesSwitch = false;
  }

  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    console.log('>>> change: ', changes);
    if (changes.device && changes.device.currentValue && changes.device.currentValue.category != '5') {
      return;
    }
    this.loadDutyCycleValue();
  }

  onUserChange(event) {
    var command = DeviceConsts.PARAM_DUTY_CYCLE;
    if (this.allDevicesSwitch) {
      command = DeviceConsts.PARAM_DUTY_CYCLE_ALL;
    }

    this.restService.setDeviceParameter(
      this.device, command, this.value).subscribe(data => {
        //data = this.convertFromBufferToJson(data);
        console.log('data: ', data);
        console.log('>>> err code: ', data.errorCode);
        if(data.errorCode){

          this.flashMessage.show('Setting device parameter failed: ' + data.result, {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else {
          console.log('>> success');
        }
      }
    );
  }

  loadDutyCycleValue() :void {
    this.restService.getDeviceParameter(this.device, DeviceConsts.PARAM_DUTY_CYCLE).subscribe(data => {

      console.log('adv pwm data: ', data);
      console.log('>>> err code: ', data.errorCode);
      if(data.errorCode){
        this.flashMessage.show('Loading device parameter failed: ' + data.result, {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else {
        this.value = data.value;
      }
    });
  }

  getStyle() : string {
    var maxBlue = 175;
    var red = 255 * Math.sin((Math.PI * this.value) / 0x1fe);
    var green = 255 * Math.sin((Math.PI * this.value) / 0x1fe);
    var blue = (this.value * maxBlue) / 255;

    return 'rgb(' + red +',' + green + ',' + blue + ')';
  }

}
