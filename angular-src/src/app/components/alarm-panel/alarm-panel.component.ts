import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services/rest.service';
import { ToolsService } from '../../services/tools.service';
import { ALARM_ARMED, ALARM_ARMING, ALARM_DISARMED } from "../../models/consts";
import { Md5 } from "md5-typescript";
import { Subscription } from "rxjs";
import { TimerObservable } from "rxjs/observable/TimerObservable";
import { WeatherSettings, TemperatureScale, ForecastMode, WeatherLayout } from 'angular-weather-widget';
import { DeviceManagerService } from '../../services/device-manager.service';
import { DeviceConsts } from '../../utils/deviceConsts';
import { Device } from '../../models/device';

@Component({
  selector: 'app-alarm-panel',
  templateUrl: './alarm-panel.component.html',
  styleUrls: ['./alarm-panel.component.css']
})
export class AlarmPanelComponent implements OnInit {

  password: string;
  displayValue: string;
  alarmArmed: boolean;
  armingSystem: boolean;
  armingCounter: number;
  smsCounter: number;
  image: any;
  slideShowOn :boolean;
  imageType : string = 'data:image/JPG;base64,';
  armStatusSubscription: Subscription;
  passwordTypingSubscription : Subscription;
  armingSystemSubscription : Subscription;
  hours: String;
  minutes: String;
  clockSubscription: Subscription;
  smsSubscription: Subscription;
  imageSubscription: Subscription;

  settings: WeatherSettings = {
    location: {
      cityName: 'Lublin'
    },
    backgroundColor: '#000000',
    color: '#adc9ff',
    width: '250px',
    height: 'auto',
    showWind: true,
    scale: TemperatureScale.CELCIUS,
    forecastMode: ForecastMode.GRID,
    showDetails: true,
    showForecast: true,
    layout: WeatherLayout.NARROW,
    language: 'pl'
  };

  constructor(
    private restService:RestService,
    private toolsService:ToolsService,
    private deviceManager: DeviceManagerService
  ) { }

  ngOnInit() {
    this.password = '';
    this.displayValue = '';
    this.armingSystem = false;
    this.armingCounter = 0;
    this.smsCounter = 0;
    this.slideShowOn = false;
    this.alarmArmed = false;
    this.startArmStatusSubscription();
    this.startClockSubscription();
    this.deviceManager.loadDevices();
  }

  ngOnDestroy() {
    this.unsubscribeArmStatusSubscription();
    this.unsubscribePasswordTypingSubscription();
    this.unsubscribeArmingSystemSubscription();
    this.unsubscribeClockSubscription();
    this.unsubscribeSmsSubscription();
    this.unsubscribeImageSubscription();
  }


  typePassword(event) {
    if (!this.armingSystem) {
      var btnLabel = event['target'].textContent;
      this.password += btnLabel;

      if (this.password.length == 1) {
        this.displayValue = '#';
      } else if (this.password.length < 10) {
        this.displayValue += '#';
      }

      this.stopTyping();
      this.startTyping();
    }

  }

  stopTyping() {
    this.unsubscribePasswordTypingSubscription();
  }

  startTyping() {
    let timer = TimerObservable.create(5000,5000);
    this.passwordTypingSubscription = timer.subscribe(t => {
      this.clearPassword();
    });
  }

  clearPassword() {
    this.stopTyping();
    this.password = '';
    this.showAlarmArmedText();
  }

  armAlarm() {
    if (!this.armingSystem) {
      this.clearPassword();
      this.restService.armSystem(1, '').subscribe(data => {
        if(data.errorCode){
        } else {
          this.armingSystem = true;
          this.displayValue = ALARM_ARMING;
          this.unsubscribeArmStatusSubscription();
          let timer = TimerObservable.create(2000,2000);
          this.armingSystemSubscription = timer.subscribe(t => {
            this.checkAlarmStatus();
          });
        }
      });
    }

  }

  disarmAlarm() {
    if (this.alarmArmed && !this.armingSystem) {
      this.restService.armSystem(0, this.getHash(this.password)).subscribe(data => {

        this.unsubscribeArmStatusSubscription();
        this.stopTyping();
        this.password = '';
        let timer;
        if(data.errorCode){
          this.displayValue = data.result.toUpperCase();
          timer = TimerObservable.create(5000, 5000);
        } else {
          timer = TimerObservable.create(0, 5000);
        }

        this.armStatusSubscription = timer.subscribe(t => {
          this.checkAlarmStatus();
        });
      });
    }
    this.stopTyping();
    this.password = '';

  }

  checkAlarmStatus() {
    this.restService.getArmSystemStatus().subscribe(data => {

      console.log('data: ', data);
      if(data.errorCode){
      } else {
        this.alarmArmed = (data.value == 1);
        if (this.armingSystem) {
          if (this.alarmArmed) {
            this.armingSystem = false;
            this.unsubscribeArmingSystemSubscription();
            this.startArmStatusSubscription();
            this.showAlarmArmedText();
          }
          this.armingCounter++;
          if (this.armingCounter > 10) {
            this.armingCounter = 0;
            this.armingSystem = false;
            this.unsubscribeArmingSystemSubscription();
            this.startArmStatusSubscription();
            this.showAlarmArmedText();
          }
        } else {
          this.showAlarmArmedText();
        }
      }
    });

  }

  getAlarmTextBackground() {
    if (this.armingSystem) {
      return `#bb8000`;
    } else {
      if (this.alarmArmed) {
        return `#C00000`;
      } else {
        return `#387e00`;
      }
    }

  }


  showAlarmArmedText() {
    if (this.password == '') {
      if (this.alarmArmed) {
        this.displayValue = ALARM_ARMED;
      } else {
        this.displayValue = ALARM_DISARMED;
      }
    }
  }


  getHash(userPasswd) : String {
    var serverPasswd = '@larmP@ssword';

    var millis = Math.floor((Date.now() / 1000)).toString();
    return Md5.init(serverPasswd + millis + userPasswd);
  }


  unsubscribeArmStatusSubscription() {
    if (this.armStatusSubscription) {
      this.armStatusSubscription.unsubscribe();
    }
  }

  unsubscribePasswordTypingSubscription() {
    if (this.passwordTypingSubscription) {
      this.passwordTypingSubscription.unsubscribe();
    }
  }

  unsubscribeArmingSystemSubscription() {
    if (this.armingSystemSubscription) {
      this.armingSystemSubscription.unsubscribe();
    }
  }

  unsubscribeClockSubscription() {
    if (this.clockSubscription) {
      this.clockSubscription.unsubscribe();
    }
  }

  unsubscribeSmsSubscription() {
    if (this.smsSubscription) {
      this.smsSubscription.unsubscribe();
    }
  }

  startArmStatusSubscription() {
    let timer = TimerObservable.create(0,5000);
    this.armStatusSubscription = timer.subscribe(t => {
      this.checkAlarmStatus();
    });
  }

  startClockSubscription() {
    let timer = TimerObservable.create(0,1000);
    this.clockSubscription = timer.subscribe(t => {
      this.setClock();
    });
  }

  startSmsSubscription() {
    let timer = TimerObservable.create(2000,2000);
    this.smsSubscription = timer.subscribe(t => {
      console.log('>>>> reset sms counter');
      this.smsCounter = 0;
      this.unsubscribeSmsSubscription();
    });
  }

  startImageSubscription() {
    let timer = TimerObservable.create(30000,30000);
    this.imageSubscription = timer.subscribe(t => {
      this.getImage();
    });
  }

  unsubscribeImageSubscription() {
    if (this.imageSubscription) {
      this.imageSubscription.unsubscribe();
    }
  }

  setClock() :void {
    var date = new Date();
    this.hours = date.getHours().toString();
    var mins = date.getMinutes();
    if (mins < 10) {
      this.minutes = '0' + mins.toString();
    } else {
      this.minutes = mins.toString();
    }
  }

  turnOffLights() {

    this.setDeviceParameter(this.deviceManager.getDevice(3, 11), DeviceConsts.PARAM_STATE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(3, 15), DeviceConsts.PARAM_STATE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(3, 17), DeviceConsts.PARAM_STATE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(3, 25), DeviceConsts.PARAM_STATE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(3, 26), DeviceConsts.PARAM_STATE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(3, 30), DeviceConsts.PARAM_STATE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(3, 31), DeviceConsts.PARAM_STATE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(3, 35), DeviceConsts.PARAM_STATE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(3, 36), DeviceConsts.PARAM_STATE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(7, 1), DeviceConsts.PARAM_MODE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(7, 10), DeviceConsts.PARAM_MODE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(7, 12), DeviceConsts.PARAM_MODE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(7, 3), DeviceConsts.PARAM_MODE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(7, 6), DeviceConsts.PARAM_MODE, 0);
    this.setDeviceParameter(this.deviceManager.getDevice(5, 0), DeviceConsts.PARAM_DUTY_CYCLE_ALL, 0);
  }

  setDeviceParameter(dev: Device, param: string, val: number) : void {
    this.restService.setDeviceParameter(dev, param, val).subscribe(data => {
      if(data.errorCode){
        console.log('Setting device parameter failed: ' + data.result);
      } else {
        console.log('Device parameter set successfully!');
      }
    });
  }

  sendSms(number: string, message: string) : void {
    this.restService.sendMessage(number, message).subscribe(data => {
      if(data.errorCode){
        console.log('Sending SMS failed: ' + data.result);
      } else {
        console.log('SMS sent successfully!');
      }
    });
  }

  getImage() : void {
    this.toolsService.getImage().subscribe(data => {
      console.log('>>>> DATA: ', data);
      this.unsubscribeImageSubscription();
      this.startImageSubscription();

      if (data['success'] && this.validateImage(data['img'])) {
        this.image = this.imageType + data['img'];
      } else {
        this.image = this.imageType + this.failImage;
      }


    });
  }

  enableSlideshow() {
    if (!this.image) {
      this.getImage();
    }

    this.startImageSubscription();

    this.slideShowOn = true;
  }

  disableSlideShow() {
    this.slideShowOn = false;
    this.unsubscribeImageSubscription();
  }

  onSendSms() {
    this.smsCounter++;
    console.log('>>> sms counter: ' + this.smsCounter);
    if (this.smsCounter == 3) {
      this.sendSms('+48788158458', 'TEST');
    }

    this.unsubscribeSmsSubscription();
    this.startSmsSubscription();

  }

  validateImage(img) {
    return (img.substring(0, 4) == '/9j/');
  }


  private failImage = '/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAOEAlgDASIAAhEBAxEB/8QAGwABAQADAQEBAAAAAAAAAAAAAAcDBQYEAgn/xAAxEAEAAQMBBwIGAwACAwEAAAAAAQIDBAUGERIhMUFRI3EUIlKBscEzYtETYQcVQqH/xAAaAQEAAwEBAQAAAAAAAAAAAAAABAYHAwIF/8QAMhEBAAECAggDCQACAwAAAAAAAAECAwYxBAUREiEiQVETYXEUFTJCYoGhssEjsQeR8P/aAAwDAQACEQMRAD8A/KoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABvti9IwtZ1evH1C3VXat2KrvBFU08U76Y5zHP/638vENCy4mXk4OTby8S9VavWp4qaqe3++zxXE1UzFM7JTtW6RZ0XTLd7SKN+imYmae8ffhPpPCXbbTbCWaMb4zQLFVNVqPUx+KauOPNO+Znf8A9d+3Prwir7M7TY20GNw1cNrMtR6trf1/tT5j8dJ7TOo2x2O+L49X0m16/Oq9Zpj+TzVTH1eY7+/WHZv1UVeHdaHiLC+jay0aNb6iiJpmNs00x+aY6THWn/rjnPwE9lwAAAAAAAAAAAAAA2WhaFma/mRjY0cNundN27MfLbp/cz2jv7b5hoWhZmv5kY2NHDbp3TduzHy26f3M9o7+2+Yqmn6fp+hafGNjRTasWomuuuuYjfy511T9uv4iEXSNIi1G7TmuuE8J166r9p0nlsU5zlvbOkeXeftHHKebR7F39Bw6c63mfFW+PgubrU0zRv6T1nl27c5jy5x0e1+1X/vLsYeJG7Cs18VMzHzXKt0xxf8AUc53R/3vnxHOOtnf3P8AJm+LiGNW06fXTqqP8UbIzmYmesxM7Z2f+jhsAHV8QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABlxMvJwcm3l4l6q1etTxU1U9v99lS2Z2mxtoMbhq4bWZaj1bW/r/AGp8x+Ok9pmUMuJl5ODk28vEvVWr1qeKmqnt/vs4X7EXo81mw1iW/h+/tjmtVfFT/Y7TH5ynpMd3tjsd8Xx6vpNr1+dV6zTH8nmqmPq8x39+s/VfZnabG2gxuGrhtZlqPVtb+v8AanzH46T2mdRtjsd8Xx6vpNr1+dV6zTH8nmqmPq8x39+sexfm3PhXVtxLhqxrax761LzRVxqpjr3mI6VR81PX1zn4CeywAAAAAAAAAAbLQtCzNfzIxsaOG3Tum7dmPlt0/uZ7R39t8w0LQszX8yMbGjht07pu3Zj5bdP7me0d/bfMVTT9P0/QtPjGxoptWLUTXXXXMRv5c66p+3X8RCLpGkRajdpzXXCeE69dV+06Ty2Kc5y3tnSPLvP2jjk0/T9P0LT4xsaKbVi1E1111zEb+XOuqft1/EQn21u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiG1u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiOaedH0eYnxLmafizFlF+j3Vqrls08JmOG9s6R9P7emYBMZ2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAy4mXk4OTby8S9VavWp4qaqe3++ypbM7TY20GNw1cNrMtR6trf1/tT5j8dJ7TMoZcTLycHJt5eJeqtXrU8VNVPb/AH2cL9iL0eazYaxLfw/f2xzWqvip/sdpj85T0mO72x2O+L49X0m16/Oq9Zpj+TzVTH1eY7+/WfqvsztNjbQY3DVw2sy1Hq2t/X+1PmPx0ntM6jbHY74vj1fSbXr86r1mmP5PNVMfV5jv79Y9i/NufCurbiXDVjW1j31qXmirjVTHXvMR0qj5qevrnPwE9lgAAAAAA2WhaFma/mRjY0cNundN27MfLbp/cz2jv7b5hoWhZmv5kY2NHDbp3TduzHy26f3M9o7+2+Yqmn6fp+hafGNjRTasWomuuuuYjfy511T9uv4iEXSNIi1G7TmuuE8J166r9p0nlsU5zlvbOkeXeftHHJp+n6foWnxjY0U2rFqJrrrrmI38uddU/br+IhPtrdrbms3JwcGqqjBonnPSb0x3nxHiPvPPdENrdrbms3JwcGqqjBonnPSb0x3nxHiPvPPdEc086Po8xPiXM0/FmLKL9HurVXLZp4TMcN7Z0j6f29MwCYzsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABlxMvJwcm3l4l6q1etTxU1U9v99lS2Z2mxtoMbhq4bWZaj1bW/r/anzH46T2mZQy4mXk4OTby8S9VavWp4qaqe3++zhfsRejzWbDWJb+H7+2Oa1V8VP8AY7TH5ynpMd3tjsd8Xx6vpNr1+dV6zTH8nmqmPq8x39+s/VfZnabG2gxuGrhtZlqPVtb+v9qfMfjpPaZ1G2Ox3xfHq+k2vX51XrNMfyeaqY+rzHf36x7F+bc+FdW3EuGrGtrHvrUvNFXGqmOveYjpVHzU9fXOfgJ7LAABstC0LM1/MjGxo4bdO6bt2Y+W3T+5ntHf23zDQtCzNfzIxsaOG3Tum7dmPlt0/uZ7R39t8xVNP0/T9C0+MbGim1YtRNdddcxG/lzrqn7dfxEIukaRFqN2nNdcJ4Tr11X7TpPLYpznLe2dI8u8/aOOTT9P0/QtPjGxoptWLUTXXXXMRv5c66p+3X8RCfbW7W3NZuTg4NVVGDRPOek3pjvPiPEfeee6IbW7W3NZuTg4NVVGDRPOek3pjvPiPEfeee6I5p50fR5ifEuZp+LMWUX6PdWquWzTwmY4b2zpH0/t6ZgExnYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADLiZeTg5NvLxL1Vq9anipqp7f77KlsztNjbQY3DVw2sy1Hq2t/X+1PmPx0ntMyhlxMvJwcm3l4l6q1etTxU1U9v99nC/Yi9Hms2GsS38P39sc1qr4qf7HaY/OU9Jju9sdjvi+PV9JtevzqvWaY/k81Ux9XmO/v1n6r7M7TY20GNw1cNrMtR6trf1/tT5j8dJ7TOo2x2O+L49X0m16/Oq9Zpj+TzVTH1eY7+/WPYvzbnwrq24lw1Y1tY99al5oq41Ux17zEdKo+anr65z9stC0LM1/MjGxo4bdO6bt2Y+W3T+5ntHf23zDQtCzNfzIxsaOG3Tum7dmPlt0/uZ7R39t8xVNP0/T9C0+MbGim1YtRNdddcxG/lzrqn7dfxEOukaRFqN2nN8HCeE69dV+06Ty2Kc5y3tnSPLvP2jjk0/T9P0LT4xsaKbVi1E1111zEb+XOuqft1/EQn21u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiG1u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiOaedH0eYnxLmafizFlF+j3Vqrls08JmOG9s6R9P7emYBMZ2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAy4mXk4OTby8S9VavWp4qaqe3++yqbMbR2tocOqubf/AB5NjdF6iInh3zv3TTPid08usbvaZnGhaFma/mRjY0cNundN27MfLbp/cz2jv7b5iqafp+n6Fp8Y2NFNqxaia6665iN/LnXVP26/iIQNNqomIp+Zqf8Ax1o2saK69Iid3Rp27Yn5p709tnWcunGcvTasWLHH/wAFm3b/AOSublfBTEcVU9ap3dZnymu1u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiO60faTS9cuXrWDdq47Mzvprjhmqn64jvH/7HeI3w0G2Ox3xfHq+k2vX51XrNMfyeaqY+rzHf368NH3bdz/LHFY8V06VrTVG/qauKrfHeinOqO0THadu2M5/Ez8B9ZhIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA2WhaFma/mRjY0cNundN27MfLbp/cz2jv7b5hoWhZmv5kY2NHDbp3TduzHy26f3M9o7+2+Yqmn6fp+hafGNjRTasWomuuuuYjfy511T9uv4iEXSNIi1G7TmuuE8J166r9p0nlsU5zlvbOkeXeftHHJp+n6foWnxjY0U2rFqJrrrrmI38uddU/br+IhPtrdrbms3JwcGqqjBonnPSb0x3nxHiPvPPdENrdrbms3JwcGqqjBonnPSb0x3nxHiPvPPdEc086Po8xPiXM0/FmLKL9HurVXLZp4TMcN7Z0j6f29M8uJl5ODk28vEvVWr1qeKmqnt/vsqWzO02NtBjcNXDazLUera39f7U+Y/HSe0zKGXEy8nBybeXiXqrV61PFTVT2/wB9nW/Yi9Hm+DhrEt/D9/bHNaq+Kn+x2mPzlPSY7vbHY74vj1fSbXr86r1mmP5PNVMfV5jv79Z+q+zO02NtBjcNXDazLUera39f7U+Y/HSe0zqNsdjvi+PV9JtevzqvWaY/k81Ux9XmO/v1j2L8258K6tuJcNWNbWPfWpeaKuNVMde8xHSqPmp6+uc/AT2WAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADZaFoWZr+ZGNjRw26d03bsx8tun9zPaO/tvmGhaFma/mRjY0cNundN27MfLbp/cz2jv7b5iqafp+n6Fp8Y2NFNqxaia6665iN/LnXVP26/iIRdI0iLUbtOa64TwnXrqv2nSeWxTnOW9s6R5d5+0ccmn6fp+hafGNjRTasWomuuuuYjfy511T9uv4iE+2t2tuazcnBwaqqMGiec9JvTHefEeI+8890Q2t2tuazcnBwaqqMGiec9JvTHefEeI+8890RzTzo+jzE+JczT8WYsov0e6tVctmnhMxw3tnSPp/b0zAJjOwAGXEy8nBybeXiXqrV61PFTVT2/32VLZnabG2gxuGrhtZlqPVtb+v9qfMfjpPaZlDLiZeTg5NvLxL1Vq9anipqp7f77OF+xF6PNZsNYlv4fv7Y5rVXxU/2O0x+cp6THd7Y7HfF8er6Ta9fnVes0x/J5qpj6vMd/frP1X2Z2mxtoMbhq4bWZaj1bW/r/anzH46T2mdRtjsd8Xx6vpNr1+dV6zTH8nmqmPq8x39+sexfm3PhXVtxLhqxrax761LzRVxqpjr3mI6VR81PX1zn4CeywAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAbLQtCzNfzIxsaOG3Tum7dmPlt0/uZ7R39t8w0LQszX8yMbGjht07pu3Zj5bdP7me0d/bfMVTT9P0/QtPjGxoptWLUTXXXXMRv5c66p+3X8RCLpGkRajdpzXXCeE69dV+06Ty2Kc5y3tnSPLvP2jjk0/T9P0LT4xsaKbVi1E1111zEb+XOuqft1/EQn21u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiG1u1tzWbk4ODVVRg0TznpN6Y7z4jxH3nnuiOaedH0eYnxLmafizFlF+j3Vqrls08JmOG9s6R9P7emYBMZ2AAAAAAy4mXk4OTby8S9VavWp4qaqe3++ypbM7TY20GNw1cNrMtR6trf1/tT5j8dJ7TMoZcTLycHJt5eJeqtXrU8VNVPb/fZwv2IvR5rNhrEt/D9/bHNaq+Kn+x2mPzlPSY7vbHY74vj1fSbXr86r1mmP5PNVMfV5jv79Z+q+zO02NtBjcNXDazLUera39f7U+Y/HSe0zqNsdjvi+PV9JtevzqvWaY/k81Ux9XmO/v1j2L8258K6tuJcNWNbWPfWpeaKuNVMde8xHSqPmp6+uc/AT2WAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADZaFoWZr+ZGNjRw26d03bsx8tun9zPaO/tvmGhaFma/mRjY0cNundN27MfLbp/cz2jv7b5iqafp+n6Fp8Y2NFNqxaia6665iN/LnXVP26/iIRdI0iLUbtOa64TwnXrqv2nSeWxTnOW9s6R5d5+0ccmn6fp+hafGNjRTasWomuuuuYjfy511T9uv4iE+2t2tuazcnBwaqqMGiec9JvTHefEeI+8890Q2t2tuazcnBwaqqMGiec9JvTHefEeI+8890RzTzo+jzE+JczT8WYsov0e6tVctmnhMxw3tnSPp/b0zAJjOwAAAAAAAAAGXEy8nBybeXiXqrV61PFTVT2/wB9lS2Z2mxtoMbhq4bWZaj1bW/r/anzH46T2mZQy4mXk4OTby8S9VavWp4qaqe3++zhfsRejzWbDWJb+H7+2Oa1V8VP9jtMfnKekx3e2Ox3xfHq+k2vX51XrNMfyeaqY+rzHf36z9V9mdpsbaDG4auG1mWo9W1v6/2p8x+Ok9pnUbY7HfF8er6Ta9fnVes0x/J5qpj6vMd/frHsX5tz4V1bcS4asa2se+tS80VcaqY695iOlUfNT19c5+AnssAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWbT9P0/QtPjGxoptWLUTXXXXMRv5c66p+3X8RCfbW7W3NZuTg4NVVGDRPOek3pjvPiPEfeee6I0lzVtTu4dOn3c+/VjU9LU1zw7uW6PaOGN0dI7PKiWdG3Kt+udsr1r7GdWsdEp0DQLfhWtmyY7/AExsyp/312RwkAlqKAAAAAAAAAAAAAAy4mXk4OTby8S9VavWp4qaqe3++ypbM7TY20GNw1cNrMtR6trf1/tT5j8dJ7TMofdi/fxbtN/GvXLVynfw10VTTVG+N3KYcL9iL0eay4bxLpGHr+2nmtVfFT/Y7T/vKekx1v8A5E0nCw8mxqGNFNu7lzX/AMtuN/zTG7545bo68+fPfE7usuPZ83OzNRvzk52TcvXJ/wDqud+6N8zujxHOeUcmB7tUTRRFNU7Xz9d6dY1lp9zStHt7lNU7dn24zPTbM8Z2d/uAOj5QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/9k=';
}
