import { Component, OnInit, Input } from '@angular/core';
import { Device } from '../../models/device';
import { DeviceManagerService } from '../../services/device-manager.service';
import { WidgetAction } from '../../models/widget-action';
import { WidgetActionSetup } from "../../models/widget-action-setup";


@Component({
  selector: 'app-widget-action-select',
  templateUrl: './widget-action-select.component.html',
  styleUrls: ['./widget-action-select.component.css']
})
export class WidgetActionSelectComponent implements OnInit {

  @Input() setup : WidgetActionSetup;
  @Input() action : WidgetAction;

  constructor(
    private deviceManager:DeviceManagerService
  ) { }

  ngOnInit() {
    this.deviceManager.loadDevices();
    if (this.setup.defaultCategory) {
      this.action.devCategory = this.setup.defaultCategory;
    }

    if (this.setup.defaultCommand) {
      this.action.devCommand = this.setup.defaultCommand;
    }

    if (!this.action.widgetEvent) {
      this.action.widgetEvent = this.setup.widgetEvent;
    }

    if (!this.action.widgetKey) {
      this.action.widgetKey = this.setup.widgetKey;
    }

  }

  getDevicesForSelectedCategory() :Device[] {
    if (this.action.devCategory) {
      var devs :Device[] = this.deviceManager.devicesList.filter((item)=>item.category == this.action.devCategory);
      console.log('>>>> devices: ', devs);
      return devs;
    }
    return new Array<Device>();
  }

  getParamsForSelectedCategory() :string[] {
    var params :string[] = new Array<string>();
    if (this.action.devCategory) {
      params = params.concat(this.deviceManager.getCategoryById(this.action.devCategory).commands);
      return params;
    }

    return params;
  }

}
