import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetActionSelectComponent } from './widget-action-select.component';

describe('WidgetActionSelectComponent', () => {
  let component: WidgetActionSelectComponent;
  let fixture: ComponentFixture<WidgetActionSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetActionSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetActionSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
