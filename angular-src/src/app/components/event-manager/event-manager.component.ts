import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services/rest.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { OperationManagerService } from '../../services/operation-manager.service';
import { DeviceManagerService } from '../../services/device-manager.service';
import { Router } from '@angular/router';
import { Device } from '../../models/device';
import { Operation, RegularOperation, DelayedOperation } from '../../models/operation';
import {SRC_PAGE_EVENT_MANAGER} from '../../models/consts';

@Component({
  selector: 'app-event-manager',
  templateUrl: './event-manager.component.html',
  styleUrls: ['./event-manager.component.css']
})
export class EventManagerComponent implements OnInit {

  filter : string;

  constructor(
    private flashMessage:FlashMessagesService,
    private restService:RestService,
    private router:Router,
    private operationManager: OperationManagerService,
    private deviceManager: DeviceManagerService
  ) { }


  ngOnInit() {
    console.log(">>>> on init");
    this.filter = '';
    this.deviceManager.loadDevices();
    this.operationManager.loadOperations();

  }

  clearFilters() {
    this.filter = '';
  }

  onActivate(operation: Operation, event) {
    this.operationManager.activateOperation(operation, true);
    event.preventDefault();
    event.stopPropagation();
  }

  onDesactivate(operation: Operation, event) {
    this.operationManager.activateOperation(operation, false);
    event.preventDefault();
    event.stopPropagation();
  }

  onSaveOperations() {
    this.operationManager.saveOperations();
  }

  onCancelOperations() {
    this.operationManager.cancelChanges();
  }

  onDeleteOperation(operation: Operation, event) {
    this.operationManager.deleteOperation(operation);
    event.preventDefault();
    event.stopPropagation();
  }

  onUndeleteOperation(operation: Operation, event) {
    this.operationManager.undeleteOperation(operation);
    event.preventDefault();
    event.stopPropagation();
  }

  getElementBackground(op : Operation) {
    if (op.type == 'delayed') {
      return `#0e3344`;
    } else {
      return ``;
    }
  }

  goToOperationPage(id: number) :void {
    this.router.navigate(['/operation', id, SRC_PAGE_EVENT_MANAGER]);
  }


}
