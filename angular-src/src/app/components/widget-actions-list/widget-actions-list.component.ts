import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WidgetActionSetup } from "../../models/widget-action-setup";
import { Location } from '@angular/common';
import { Device } from '../../models/device';
import { DeviceManagerService } from '../../services/device-manager.service';
import {FlashMessagesService} from "angular2-flash-messages";
import {ActionsWidgetsService} from "../../services/actions-widgets.service";
import {ActionWidget} from "../../models/action-widget";
import {WidgetAction} from "../../models/widget-action";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { BsModalService } from 'ngx-bootstrap/modal';
import {WIDGET_TYPES} from "../../models/consts";
import {ActionsWidgetsComponent} from "../actions-widgets/actions-widgets.component";

@Component({
  selector: 'app-widget-actions-list',
  templateUrl: './widget-actions-list.component.html',
  styleUrls: ['./widget-actions-list.component.css']
})
export class WidgetActionsListComponent implements OnInit {

  bsModalRef: BsModalRef;
  actionSetup : WidgetActionSetup;
  groupkey : number;
  widget : ActionWidget;
  actions : WidgetAction[];
  newAction : WidgetAction;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public flashMessage: FlashMessagesService,
    private deviceManager:DeviceManagerService,
    public actionWidgetsService : ActionsWidgetsService,
    private router : Router,
    private modalService: BsModalService

  ) { }

  ngOnInit() {
    this.deviceManager.loadDevices();
    this.route.queryParams.subscribe(params => {
      console.log('params: ', params);
      this.actionSetup = JSON.parse(params.actionsetup);
      this.groupkey = params.groupkey;

      // this.location.replaceState(this.location.path().split('?')[0], '');
      this.loadWidget();
    });
  }

  back() {
    this.router.navigate(['/widgets', 1, this.groupkey]);
  }

  loadWidget() {
    this.actionWidgetsService.getWidget(this.actionSetup.widgetKey, (callback => {
      this.widget = callback;
      this.actions = new Array<WidgetAction>();
      for (var action of this.widget.actions) {
        if (action.widgetEvent == this.actionSetup.widgetEvent) {
          this.actions.push(action);
        }
      }
    }))
  }

  deleteAction(action : WidgetAction) {
    this.actionWidgetsService.deleteWidgetAction(action.key, (callback => {
      if (callback['success'] === false){
        this.flashMessage.show('Delete widget action failed: ' + callback['msg'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else if (callback['success'] === true) {
        this.flashMessage.show('Widget action deleted', {
          cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});

        this.loadWidget();
      }
    }));
  }

  createAction() {
    this.newAction = new WidgetAction();
    this.showActionSelectPopup(this.newAction);
  }

  showActionSelectPopup(action : WidgetAction) {
      this.bsModalRef = this.modalService.show(WidgetActionPopupComponent);
      this.bsModalRef.content.closeBtnName = 'Close';
      this.bsModalRef.content.parentController = this;
      this.bsModalRef.content.popupType = 'widget_action';
      this.bsModalRef.content.setup = this.actionSetup;
      this.bsModalRef.content.action = JSON.parse(JSON.stringify(action));
  }

  onDeleteActionButton(action : WidgetAction, event) {
    this.bsModalRef = this.modalService.show(WidgetActionPopupComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
    this.bsModalRef.content.parentController = this;
    this.bsModalRef.content.popupType = 'widget_action_delete';
    this.bsModalRef.content.action = action;
    event.preventDefault();
    event.stopPropagation();
  }

}


@Component({
  selector: 'modal-content',
  styleUrls: ['./widget-actions-list.component.css'],
  template: `
     
      
      <div *ngIf="popupType == 'widget_action_delete'">
        <div class="modal-header">
          <h4 class="modal-title pull-left modal-header-custom">WARNING</h4>
        </div>
        <div class="modal-body modal-content-center">
            <div>
                Do you really want to remove the widget action?
            </div>
            <div modal-input-section class="modal-content-center mt-3">
                <button type="button" class="btn btn-danger wide-button mr-2" onclick="this.blur();" (click)="onPopupCancel()">No</button>
                <button type="button" class="btn btn-success" onclick="this.blur();"  (click)="onDeleteWidgetAction()">Yes</button>
            </div>
        </div>
      </div>

     

      <div *ngIf="popupType == 'widget_action'">
        <div class="modal-header">
          <h4 class="modal-title pull-left modal-header-custom">Edit widget action</h4>
        </div>
        <div class="modal-body">
          <div class="mt-3">
            <app-widget-action-select [(setup)]="setup" [(action)]="action"></app-widget-action-select> 
          </div>
          <div modal-input-section class="modal-content-center mt-5">
            <button type="button" class="btn btn-default mr-2" onclick="this.blur();" (click)="onPopupCancel()">Cancel</button>
            <button type="button" class="btn btn-success" onclick="this.blur();"  (click)="onSaveWidgetAction()">Save</button>
          </div>
        </div>
      </div>
    
  `
})


export class WidgetActionPopupComponent implements OnInit {

  parentController : WidgetActionsListComponent;
  widget : ActionWidget;
  popupType : string;
  setup : WidgetActionSetup;
  action : WidgetAction;

  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit() {
  }



  onSaveWidgetAction() {
    if (!this.action.key) {
      this.parentController.actionWidgetsService.createNewWidgetAction(this.action, (callback => {
        if (callback['success'] === false){
          this.parentController.flashMessage.show('Creating widget action failed: ' + callback['msg'], {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else if (callback['success'] === true) {
          this.parentController.flashMessage.show('Widget action created', {
            cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});

          this.parentController.loadWidget();
        }
      }));
    } else {
      this.parentController.actionWidgetsService.updateWidgetAction(this.action, (callback => {
        if (callback['success'] === false){
          this.parentController.flashMessage.show('Updating widget action failed: ' + callback['msg'], {
            cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});
        } else if (callback['success'] === true) {
          this.parentController.flashMessage.show('Widget action updated', {
            cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
            timeout: 5000});

          this.parentController.loadWidget();
        }
      }));
    }
    this.bsModalRef.hide();
  }



  onDeleteWidgetAction() : void {
    this.parentController.deleteAction(this.action);
    this.bsModalRef.hide();
  }

  onPopupCancel() : void {
    this.bsModalRef.hide();
  }
}
