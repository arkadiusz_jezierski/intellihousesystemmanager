import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetActionsListComponent } from './widget-actions-list.component';

describe('WidgetActionsListComponent', () => {
  let component: WidgetActionsListComponent;
  let fixture: ComponentFixture<WidgetActionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetActionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetActionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
