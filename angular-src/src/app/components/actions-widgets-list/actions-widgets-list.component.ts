import { Component, OnInit } from '@angular/core';
import { ActionGroup } from '../../models/action-group';
import { ActionsWidgetsService } from '../../services/actions-widgets.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";
import { FlashMessagesService } from 'angular2-flash-messages';
import { Location } from '@angular/common';

@Component({
  selector: 'app-actions-widgets-list',
  templateUrl: './actions-widgets-list.component.html',
  styleUrls: ['./actions-widgets-list.component.css']
})
export class ActionsWidgetsListComponent implements OnInit {

	bsModalRef: BsModalRef;
  flashMessageCss : string;
  flashMessageTxt : string;

  constructor(
  	private actionWidgetsService : ActionsWidgetsService,
  	private modalService: BsModalService,
    private router:Router,
    private route: ActivatedRoute,
    private flashMessage: FlashMessagesService,
    private location: Location
  ) { 
   
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.flashMessageCss = params["cssClass"];
      this.flashMessageTxt = params["message"];
    
      if (this.flashMessageCss && this.flashMessageTxt) {
        this.flashMessage.show(this.flashMessageTxt, {
          cssClass: this.flashMessageCss,
          timeout: 5000});
      }

      this.location.replaceState(this.location.path().split('?')[0], '');
    });

  	this.actionWidgetsService.loadActionGroups();
  }

  goToActionsGroupPage(key : number) {
  	this.router.navigate(['/widgets', 0, key]);
  }

  goToEditActionsGroupPage(key : number) {
  	this.router.navigate(['/widgets', 1, key]);
  }

  onDeleteGroupPage(actionGroup : ActionGroup, event) {
  	this.showPopup(actionGroup.key);
    event.preventDefault();
    event.stopPropagation();
  }

  showPopup(key: number) {
    // this.popupRequest = request;
    this.bsModalRef = this.modalService.show(ActionGroupDeletePopupComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
    this.bsModalRef.content.parentController = this;
    this.bsModalRef.content.actionGroupKey = key;
  }

  deleteActionWidgetsGroup(key : number) {
  	this.actionWidgetsService.deleteActionWidgetsGroup(key, ( callback=> {
      this.actionWidgetsService.loadActionGroups();
    }));
  	
  }

  createActionGroup() {
  	this.actionWidgetsService.createNewActionGroup('New Actions Group');
  	this.actionWidgetsService.loadActionGroups();
  }

}

@Component({
  selector: 'modal-content',
  styleUrls: ['./actions-widgets-list.component.css'],
  template: `
    <div class="modal-header">
      <h4 class="modal-title pull-left modal-header-custom">WARNING</h4>
    </div>
    <div class="modal-body modal-body-custom">
    <div class="modal-content-center">
        Do you really want to delete actions group?
    </div>
    <div modal-input-section class="modal-content-center mt-3">
        <button type="button" class="btn btn-danger wide-button mr-2" onclick="this.blur();" (click)="onPopupCancel()">No</button>
        <button type="button" class="btn btn-success" onclick="this.blur();"  (click)="onPopupConfirm()">Yes</button>
    </div>
    </div>
  `
})


export class ActionGroupDeletePopupComponent implements OnInit {

  parentController : ActionsWidgetsListComponent;
  actionGroupKey: number;

  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit() {  }

  onPopupConfirm() : void {
    this.parentController.deleteActionWidgetsGroup(this.actionGroupKey);
    this.bsModalRef.hide();
  }

  onPopupCancel() : void {
    this.bsModalRef.hide();
  }
}
