import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services/rest.service';
import { ConfigService } from '../../services/config.service';
import { DeviceManagerService } from '../../services/device-manager.service';
import { Device } from '../../models/device';
import { DeviceConsts } from '../../utils/deviceConsts';
import { ConfigConsts } from '../../utils/configConsts';
import { Router } from '@angular/router';
import { Subscription } from "rxjs";
import { TimerObservable } from "rxjs/observable/TimerObservable";


@Component({
  selector: 'app-living-room',
  templateUrl: './living-room.component.html',
  styleUrls: ['./living-room.component.css']
})


export class LivingRoomComponent implements OnInit {

  currentDevices: Object = {
    kitchenDevices :[] = [
      DeviceConsts.KITCHEN_MAIN,
      DeviceConsts.KITCHEN_SIDE,
      DeviceConsts.KITCHEN_FURN_LEFT,
      DeviceConsts.KITCHEN_FURN_RIGHT
    ],

    livingroomlightsDevices :[] = [
      DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_01,
      DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_02,
      DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_03,
      DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_04,
      DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_05,
      DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_06,
      DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_07,
      DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_08,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_01,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_02,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_03,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_04,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_05,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_06,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_07,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_08,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_09,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_10,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_11,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_12,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_13,
      DeviceConsts.LIVINGROOM_LIGHTS_RIM_14
    ],

    livingroomWallEffectDevices :[] = [
      DeviceConsts.LIVINGROOM_WALL_EFFECT_TOP,
      DeviceConsts.LIVINGROOM_WALL_EFFECT_MIDDLE,
      DeviceConsts.LIVINGROOM_WALL_EFFECT_BOTTOM
    ],

    livingroomCeilingEffectDevices :[] = [
      DeviceConsts.LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT,
      DeviceConsts.LIVINGROOM_CEILING_EFFECT_OUTSIDE_LONG,
      DeviceConsts.LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT,
      DeviceConsts.LIVINGROOM_CEILING_EFFECT_INSIDE_LONG
    ]
  };

  devicesMap: Object;
  buttonNames: Object;
  hours: String;
  minutes: String;
  subscription: Subscription;


  constructor(
    private router:Router,
    private restService:RestService,
    private deviceManager: DeviceManagerService,
    private configService: ConfigService
  ) { }

  ngOnInit() {
    let timer = TimerObservable.create(0,1000);
    this.subscription = timer.subscribe(t => {
      this.setClock();
    });

    this.loadLivingRoomLightsSetNames();
    this.deviceManager.loadDevices(() => {this.loadDevicesConfig();});
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  setClock() :void {
    var date = new Date();
    this.hours = date.getHours().toString();
    var mins = date.getMinutes();
    if (mins < 10) {
      this.minutes = '0' + mins.toString();
    } else {
      this.minutes = mins.toString();
    }
  }


  loadDevicesConfig(): void {
    console.log('>>> loading devices config');
    this.getDevices();
  }

  loadLivingRoomLightsSetNames() :void {
    this.buttonNames = new Object();
    for (var i = 1; i < 5; i++) {
      this.loadSetName(ConfigConsts.LIVINGROOM_LIGHTS_SET + i, (name, id) => {
        this.buttonNames[id] = name;
      });
    }
  }

  loadSetName(id: string, callback: (val: number, id: string) => void) :void {
    this.configService.getBasicConfig(id).subscribe(data => {
      if (data.success) {
        callback(data.config.value, id);
      } else {
        console.log('Loading LightSetName ' + id +  ' error: ', data.msg);
      }

    });
  }

  getAllCurrentDevices() :string[] {
    return this.currentDevices['kitchenDevices']
      .concat(this.currentDevices['livingroomlightsDevices'])
      .concat(this.currentDevices['livingroomWallEffectDevices'])
      .concat(this.currentDevices['livingroomCeilingEffectDevices']);
  }

  goToLivingRoomLightsPage(id: number) :void {
    this.router.navigate(['/livingroomlights', ConfigConsts.LIVINGROOM_LIGHTS_SET + id]);
  }

  setLivingRoomLightsFromConfig(setId :number) :void {
    var configName = ConfigConsts.LIVINGROOM_LIGHTS_SET + setId;
    this.configService.getDevicesConfigs(configName).subscribe(data => {
      console.log('data: ', data);
      if (data.success) {
        console.log('config: ', data.configs);
        for (var config of data.configs) {

          var key = this.getDeviceKey(config.address, config.category);

          this.setDeviceParameter(this.devicesMap[key], DeviceConsts.PARAM_DUTY_CYCLE, config.value);
          console.log('Setting device ' + key + ' duty cycle from config = ' + config.value);
        }

      } else {
        console.log('GetDevicesConfigs error: ', data.msg);
      }

    });
  }

  getDeviceKey(address: string, category: string) :string{
    for (var devKey of this.getAllCurrentDevices()) {
      if (this.devicesMap[devKey].address == address && this.devicesMap[devKey].category == category) {
        return devKey;
      }
    }
  }

  switchKitchenMainLights(): void {
    console.log('>>> kuchnia sufit');
    this.setDeviceParameter(this.devicesMap[DeviceConsts.KITCHEN_MAIN], DeviceConsts.PARAM_SWITCH, 0);
  }

  switchKitchenSideLigths(): void {
    console.log('>>> kuchnia boczne');
    this.setDeviceParameter(this.devicesMap[DeviceConsts.KITCHEN_SIDE], DeviceConsts.PARAM_SWITCH, 0);
  }

  switchKitchenFurnitureLights(): void {
    console.log('>>> kuchnia szafki');
    this.loadDeviceParameter(this.devicesMap[DeviceConsts.KITCHEN_FURN_RIGHT], DeviceConsts.PARAM_STATE, (val_r) => {
      this.loadDeviceParameter(this.devicesMap[DeviceConsts.KITCHEN_FURN_LEFT], DeviceConsts.PARAM_STATE, (val_l) => {
        if (val_l > 0 || val_r > 0) {
          this.setDeviceParameter(this.devicesMap[DeviceConsts.KITCHEN_FURN_LEFT], DeviceConsts.PARAM_STATE, 0);
          this.setDeviceParameter(this.devicesMap[DeviceConsts.KITCHEN_FURN_RIGHT], DeviceConsts.PARAM_STATE, 0);
        } else {
          this.setDeviceParameter(this.devicesMap[DeviceConsts.KITCHEN_FURN_LEFT], DeviceConsts.PARAM_STATE, 1);
          this.setDeviceParameter(this.devicesMap[DeviceConsts.KITCHEN_FURN_RIGHT], DeviceConsts.PARAM_STATE, 1);
        }
      });
    });
  }

  switchAllLightsOff() :void {
    // this.saveSettings();
    this.livingroomLightsOff();
    this.livingroomWallEffectOff();
    this.livingroomCeilingEffectOff();
    this.setDeviceParameter(this.devicesMap[DeviceConsts.KITCHEN_MAIN], DeviceConsts.PARAM_STATE, 0);
    this.setDeviceParameter(this.devicesMap[DeviceConsts.KITCHEN_SIDE], DeviceConsts.PARAM_STATE, 0);
    this.setDeviceParameter(this.devicesMap[DeviceConsts.KITCHEN_FURN_LEFT], DeviceConsts.PARAM_STATE, 0);
    this.setDeviceParameter(this.devicesMap[DeviceConsts.KITCHEN_FURN_RIGHT], DeviceConsts.PARAM_STATE, 0);

  }

  increaseAllLivingRoomLights() :void {
    this.setDeviceParameter(this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_01], DeviceConsts.PARAM_DUTY_CYCLE_UP_ALL, 20);
    this.setDeviceParameter(this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_01], DeviceConsts.PARAM_DUTY_CYCLE_UP_ALL, 20);
  }

  decreaseAllLivingRoomLights() :void {
    this.setDeviceParameter(this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_01], DeviceConsts.PARAM_DUTY_CYCLE_DOWN_ALL, 20);
    this.setDeviceParameter(this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_01], DeviceConsts.PARAM_DUTY_CYCLE_DOWN_ALL, 20);
  }

  loadDeviceParameter(dev: Device, param: string, callback: (val: number) => void) :void {
    this.restService.getDeviceParameter(dev, param).subscribe(data => {
      if(data.errorCode){
        console.log('Loading device parameters failed: ' + data.result);
      } else {
        callback(data.value);
      }
    });
  }

  setDeviceParameter(dev: Device, param: string, val: number) : void {
    this.restService.setDeviceParameter(dev, param, val).subscribe(data => {
      if(data.errorCode){
        console.log('Setting device parameter failed: ' + data.result);
      } else {
        console.log('Device parameter set successfully!');
      }
    });
  }



  // getDevice(): void {
  //   console.log('>>>> get device');
  //   this.configService.getDevice('kuchnia_sufit').subscribe(data => {
  //
  //     console.log('data: ', data);
  //     console.log('>>> err success: ', data.success);
  //     console.log('>>> err msg: ', data.msg);
  //     console.log('>>> err dev: ', data.device);
  //
  //   });
  // }

  testLoadSettings(): void {
    console.log('>>> LOAD SETTINGS');

    // this.configService.getBasicConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1).subscribe(data => {
    //   console.log('data: ', data);
    //   if (data.success) {
    //     console.log('config: ', data.config);
    //
    //   } else {
    //     console.log('GetDevices error: ', data.msg);
    //   }
    //
    //
    // });

    this.configService.getDevicesConfigs(ConfigConsts.LIVINGROOM_LIGHTS_SET_1).subscribe(data => {
      console.log('data: ', data);
      if (data.success) {
        console.log('config: ', data.configs);

      } else {
        console.log('GetDevices error: ', data.msg);
      }


    });
  }

  testDelSettings(): void {
    console.log('>>> DELETE SETTINGS');


    this.configService.removeDevicesConfigs().subscribe(data => {
      console.log('DELETE DEVICES CONFIG: ', data);

    });

    this.configService.removeDevices().subscribe(data => {
      console.log('DELETE DEVICES: ', data);

    });
  }

  getDevices(): void {
    console.log('>>>> get devices');

    this.configService.getDevices(this.getAllCurrentDevices()).subscribe(data => {
      console.log('data: ', data);
      if (data.success) {
        console.log('GetDevices success, devices list: ', data.devices);
        this.devicesMap = new Object();

        for (var dev of data.devices) {
          console.log('dev:' ,dev);
          this.devicesMap[dev.key] = this.deviceManager.getDevice(dev.category, dev.address);
        }
        console.log('>>>> devices map: ', this.devicesMap);
      } else {
        console.log('GetDevices error: ', data.msg);
      }


    });
  }

  removeDevices(): void {
    console.log('>>>> remove devices');
    this.configService.removeDevices().subscribe(data => {

      console.log('data: ', data);

    });
  }

  saveSettings(): void {
    console.log('>>> saving settings');
    this.configService.addDevice(this.deviceManager.getDevice(3,30), 'kuchnia_sufit').subscribe(data=>{
      console.log('>>>> save device - kuchnia sufit');
    });
    this.configService.addDevice(this.deviceManager.getDevice(3,31), 'kuchnia_boczne').subscribe(data=>{
      console.log('>>>> save device - kuchnia boczne');
    });
    this.configService.addDevice(this.deviceManager.getDevice(3,35), 'kuchnia_szafki_prawe').subscribe(data=>{
      console.log('>>>> save device - kuchnia szafki prawe');
    });
    this.configService.addDevice(this.deviceManager.getDevice(3,36), 'kuchnia_szafki_lewe').subscribe(data=>{
      console.log('>>>> save device - kuchnia szafki lewe');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,1), 'salon_zew_10').subscribe(data=>{
      console.log('>>>> save device - salon_zew_10');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,2), 'salon_zew_09').subscribe(data=>{
      console.log('>>>> save device - salon_zew_09');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,21), 'salon_zew_12').subscribe(data=>{
      console.log('>>>> save device - salon_zew_12');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,22), 'salon_zew_11').subscribe(data=>{
      console.log('>>>> save device - salon_zew_11');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,13), 'salon_zew_04').subscribe(data=>{
      console.log('>>>> save device - salon_zew_04');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,14), 'salon_zew_03').subscribe(data=>{
      console.log('>>>> save device - salon_zew_03');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,15), 'salon_zew_02').subscribe(data=>{
      console.log('>>>> save device - salon_zew_02');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,16), 'salon_zew_01').subscribe(data=>{
      console.log('>>>> save device - salon_zew_01');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,17), 'salon_zew_14').subscribe(data=>{
      console.log('>>>> save device - salon_zew_14');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,18), 'salon_zew_13').subscribe(data=>{
      console.log('>>>> save device - salon_zew_13');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,3), 'salon_zew_08').subscribe(data=>{
      console.log('>>>> save device - salon_zew_08');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,4), 'salon_zew_07').subscribe(data=>{
      console.log('>>>> save device - salon_zew_07');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,5), 'salon_zew_06').subscribe(data=>{
      console.log('>>>> save device - salon_zew_06');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,6), 'salon_zew_05').subscribe(data=>{
      console.log('>>>> save device - salon_zew_05');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,10), 'salon_wew_07').subscribe(data=>{
      console.log('>>>> save device - salon_wew_07');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,11), 'salon_wew_04').subscribe(data=>{
      console.log('>>>> save device - salon_wew_04');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,12), 'salon_wew_01').subscribe(data=>{
      console.log('>>>> save device - salon_wew_01');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,19), 'salon_wew_02').subscribe(data=>{
      console.log('>>>> save device - salon_wew_02');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,20), 'salon_wew_06').subscribe(data=>{
      console.log('>>>> save device - salon_wew_06');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,7), 'salon_wew_03').subscribe(data=>{
      console.log('>>>> save device - salon_wew_03');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,8), 'salon_wew_05').subscribe(data=>{
      console.log('>>>> save device - salon_wew_05');
    });
    this.configService.addDevice(this.deviceManager.getDevice(5,9), 'salon_wew_08').subscribe(data=>{
      console.log('>>>> save device - salon_wew_08');
    });
    this.configService.addDevice(this.deviceManager.getDevice(3,41), DeviceConsts.KITCHEN_SOCKET_01).subscribe(data=>{
      console.log('>>>> save device - kuchnia_gniazdko_01');
    });
    this.configService.addDevice(this.deviceManager.getDevice(3,42), DeviceConsts.KITCHEN_SOCKET_02).subscribe(data=>{
      console.log('>>>> save device - kuchnia_gniazdko_02');
    });
    this.configService.addDevice(this.deviceManager.getDevice(3,39), DeviceConsts.KITCHEN_SOCKET_03).subscribe(data=>{
      console.log('>>>> save device - kuchnia_gniazdko_03');
    });
    this.configService.addDevice(this.deviceManager.getDevice(3,40), DeviceConsts.KITCHEN_SOCKET_04).subscribe(data=>{
      console.log('>>>> save device - kuchnia_gniazdko_04');
    });
    this.configService.addDevice(this.deviceManager.getDevice(3,37), DeviceConsts.KITCHEN_SOCKET_05).subscribe(data=>{
      console.log('>>>> save device - kuchnia_gniazdko_05');
    });
    this.configService.addDevice(this.deviceManager.getDevice(3,38), DeviceConsts.KITCHEN_SOCKET_06).subscribe(data=>{
      console.log('>>>> save device - kuchnia_gniazdko_06');
    });
    this.configService.addDevice(this.deviceManager.getDevice(3,5), DeviceConsts.LIVINGROOM_SOCKET_01).subscribe(data=>{
      console.log('>>>> save device - salon_gniazdko_01');
    });
    this.configService.addDevice(this.deviceManager.getDevice(7,5), DeviceConsts.LIVINGROOM_WALL_EFFECT_TOP).subscribe(data=>{
      console.log('>>>> save device - LIVINGROOM_WALL_EFFECT_TOP');
    });
    this.configService.addDevice(this.deviceManager.getDevice(7,4), DeviceConsts.LIVINGROOM_WALL_EFFECT_MIDDLE).subscribe(data=>{
      console.log('>>>> save device - LIVINGROOM_WALL_EFFECT_MIDDLE');
    });
    this.configService.addDevice(this.deviceManager.getDevice(7,3), DeviceConsts.LIVINGROOM_WALL_EFFECT_BOTTOM).subscribe(data=>{
      console.log('>>>> save device - LIVINGROOM_WALL_EFFECT_BOTTOM');
    });
    this.configService.addDevice(this.deviceManager.getDevice(7,2), DeviceConsts.LIVINGROOM_CEILING_EFFECT_INSIDE_LONG).subscribe(data=>{
      console.log('>>>> save device - LIVINGROOM_CEILING_EFFECT_INSIDE_LONG');
    });
    this.configService.addDevice(this.deviceManager.getDevice(7,1), DeviceConsts.LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT).subscribe(data=>{
      console.log('>>>> save device - LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT');
    });
    this.configService.addDevice(this.deviceManager.getDevice(7,6), DeviceConsts.LIVINGROOM_CEILING_EFFECT_OUTSIDE_LONG).subscribe(data=>{
      console.log('>>>> save device - LIVINGROOM_CEILING_EFFECT_OUTSIDE_LONG');
    });
    this.configService.addDevice(this.deviceManager.getDevice(7,7), DeviceConsts.LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT).subscribe(data=>{
      console.log('>>>> save device - LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT');
    });


    this.configService.addBasicConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, 'SET1').subscribe(data=>{
      console.log('>>>> save basic setting - LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addBasicConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, 'SET2').subscribe(data=>{
      console.log('>>>> save basic setting LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addBasicConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, 'SET3').subscribe(data=>{
      console.log('>>>> save basic setting LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addBasicConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, 'SET4').subscribe(data=>{
      console.log('>>>> save basic setting LIVINGROOM_LIGHTS_SET_4' );
    });

    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_01], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_02], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_03], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_04], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_05], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_06], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_07], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_08], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_09], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_10], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_11], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_12], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_13], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_14], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_01], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_02], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_03], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_04], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_05], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_06], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_07], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_08], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_1');
    });

    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_01], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_02], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_03], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_04], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_05], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_06], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_07], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_08], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_09], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_10], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_11], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_12], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_13], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_14], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_01], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_02], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_03], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_04], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_05], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_06], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_07], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_08], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_2');
    });

    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_01], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_02], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_03], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_04], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_05], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_06], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_07], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_08], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_09], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_10], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_11], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_12], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_13], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_14], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_01], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_02], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_03], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_04], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_05], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_06], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_07], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_08], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_3');
    });

    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_01], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_02], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_03], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_04], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_05], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_06], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_07], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_08], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_09], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_10], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_11], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_12], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_13], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_14], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_01], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_02], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_03], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_04], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_05], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_06], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_07], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_LIGHTS_SET_4, this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_08], DeviceConsts.PARAM_DUTY_CYCLE, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_LIGHTS_SET_4');
    });

    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_WALL_EFFECT_TOP], DeviceConsts.PARAM_RGB, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_WALL_EFFECT_MIDDLE], DeviceConsts.PARAM_RGB, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_1');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_1, this.devicesMap[DeviceConsts.LIVINGROOM_WALL_EFFECT_BOTTOM], DeviceConsts.PARAM_RGB, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_1');
    });

    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_WALL_EFFECT_TOP], DeviceConsts.PARAM_RGB, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_WALL_EFFECT_MIDDLE], DeviceConsts.PARAM_RGB, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_2');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_2, this.devicesMap[DeviceConsts.LIVINGROOM_WALL_EFFECT_BOTTOM], DeviceConsts.PARAM_RGB, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_2');
    });

    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_WALL_EFFECT_TOP], DeviceConsts.PARAM_RGB, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_WALL_EFFECT_MIDDLE], DeviceConsts.PARAM_RGB, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_3');
    });
    this.configService.addDeviceConfig(ConfigConsts.LIVINGROOM_WALL_EFFECT_SET_3, this.devicesMap[DeviceConsts.LIVINGROOM_WALL_EFFECT_BOTTOM], DeviceConsts.PARAM_RGB, 0).subscribe(data=>{
      console.log('>>>> save device config LIVINGROOM_WALL_EFFECT_SET_3');
    });


    // this.configService.addDevice(this.deviceManager.getDevice(3,35), 'kuchnia_szafki_prawe');
    // this.configService.addDevice(this.deviceManager.getDevice(3,36), 'kuchnia_szafki_lewe');
  }


  livingroomLightsOff() :void {
    console.log('Turn living room lights off');

    this.setDeviceParameter(
      this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_RIM_01],
      DeviceConsts.PARAM_DUTY_CYCLE_ALL,
      0
    );

    this.setDeviceParameter(
      this.devicesMap[DeviceConsts.LIVINGROOM_LIGHTS_MIDDLE_01],
      DeviceConsts.PARAM_DUTY_CYCLE_ALL,
      0
    );

  }

  livingroomWallEffectOff() :void {
    console.log('Turn living room wall effect off');

    this.setDeviceParameter(this.devicesMap[DeviceConsts.LIVINGROOM_WALL_EFFECT_TOP], DeviceConsts.PARAM_MODE, 0);
  }

  livingroomCeilingEffectOff() :void {
    console.log('Turn living room ceiling effect off');

    this.setDeviceParameter(this.devicesMap[DeviceConsts.LIVINGROOM_CEILING_EFFECT_OUTSIDE_SHORT], DeviceConsts.PARAM_MODE, 0);
    this.setDeviceParameter(this.devicesMap[DeviceConsts.LIVINGROOM_CEILING_EFFECT_INSIDE_SHORT], DeviceConsts.PARAM_MODE, 0);
  }


  unblockSwitch() :void{
    var i = 0;
    while(i < 1000) {
      i++;
      // this.setDeviceParameter(this.deviceManager.getDevice(3, 26), DeviceConsts.PARAM_STATE, 0);
     // this.loadDeviceParameter(this.deviceManager.getDevice(3, 26), DeviceConsts.PARAM_STATE, (val) => {
       // if (val > 0) {
          this.setDeviceParameter(this.deviceManager.getDevice(3, 26), DeviceConsts.PARAM_STATE, 0);
       // } else {
          this.setDeviceParameter(this.deviceManager.getDevice(3, 26), DeviceConsts.PARAM_STATE, 1);
      //  }
      //});

    }
  }

}
