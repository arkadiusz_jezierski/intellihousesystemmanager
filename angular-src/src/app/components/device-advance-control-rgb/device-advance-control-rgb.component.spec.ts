import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceAdvanceControlRgbComponent } from './device-advance-control-rgb.component';

describe('DeviceAdvanceControlRgbComponent', () => {
  let component: DeviceAdvanceControlRgbComponent;
  let fixture: ComponentFixture<DeviceAdvanceControlRgbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceAdvanceControlRgbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceAdvanceControlRgbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
