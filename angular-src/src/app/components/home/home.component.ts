import { Component, OnInit } from '@angular/core';
import { ToolsService } from '../../services/tools.service';
import { Subscription } from "rxjs";
import { TimerObservable } from "rxjs/observable/TimerObservable";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { RestService } from '../../services/rest.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  logsArray: string[];
  logsSubscription: Subscription;
  logsSizeSubscription: Subscription;
  bsModalRef: BsModalRef;
  logsSize: string;
  logsFontSize: string;
  logFilter: string;
  minFilterLength: number;

  username: String;
  password: String;
  rememberme: Boolean;

  constructor(
    private toolsService:ToolsService,
    private modalService: BsModalService,
    private flashMessage: FlashMessagesService,
    private router:Router,
    private authService:AuthService
  ) { }

  ngOnInit() {
    this.logFilter = '';
    this.logsArray = [];
    this.startLogsSubscription();
    this.startLogsSizeSubscription();
    this.logsFontSize = '';
    this.minFilterLength = 2;
  }

  ngOnDestroy() {
    this.unsubscribeLogsSubscription();
    this.unsubscribeLogsSizeSubscription();
  }

  onFilterChange(event) :void {
    console.log('>>> filter: ', this.logFilter);
    if (this.logFilter.length > 2) {
      this.logsArray = [];
    }

    this.getLatestLogs();
  }

  getLatestLogs() : void {
    var lastLogId = '';
    if (this.logsArray && this.logsArray.length > 0) {
      lastLogId = this.logsArray[0]['_id'];
    }

    var searchFilter = '';

    if (this.logFilter.length > this.minFilterLength) {
      searchFilter = this.logFilter;
    }

    this.toolsService.getLatestLogs(lastLogId, searchFilter).subscribe(data => {

      if (data['success']) {
        this.logsArray = data['logs'].concat(this.logsArray);
      } else {
        console.log('>>> getLatestLogs failure: ', data);
        this.flashMessage.show('Loading latest logs failed: ' + data['msg'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      }

    });
  }

  getOlderLogs() : void {
    var oldestLogId = '';
    if (this.logsArray && this.logsArray.length > 0) {
      oldestLogId = this.logsArray[this.logsArray.length - 1]['_id'];

      var searchFilter = '';

      if (this.logFilter.length > this.minFilterLength) {
        searchFilter = this.logFilter;
      }

      this.toolsService.getOlderLogs(oldestLogId, searchFilter).subscribe(data => {

        if (data['success']) {
          this.logsArray = this.logsArray.concat(data['logs']);
        } else {
          console.log('>>> getOlderLogs failure: ', data);
          this.flashMessage.show('Loading older logs failed: ' + data['result'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
        }

      });
    }


  }

  getLogsQnty(): void {
    this.toolsService.getLogsQnty().subscribe(data => {

      if (data['success']) {
        this.logsSize = data['logSize'];
      } else {
        console.log('>>> getLogsQnty failure: ', data);
        this.flashMessage.show('Loading logs qnty failed: ' + data['result'], {
        cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
        timeout: 5000});
      }

    });
  }

  startLogsSubscription() {
    let timer = TimerObservable.create(0,5000);
    this.logsSubscription = timer.subscribe(t => {
      this.getLatestLogs();
    });
  }

  startLogsSizeSubscription() {
    let timer = TimerObservable.create(0,60000);
    this.logsSizeSubscription = timer.subscribe(t => {
      this.getLogsQnty();
    });
  }

  unsubscribeLogsSubscription() {
    if (this.logsSubscription) {
      this.logsSubscription.unsubscribe();
    }
  }

  unsubscribeLogsSizeSubscription() {
    if (this.logsSizeSubscription) {
      this.logsSizeSubscription.unsubscribe();
    }
  }

  onScroll(event) {
    var srcElement = event.srcElement;
    if (srcElement && srcElement.scrollHeight <= (srcElement.scrollTop + srcElement.clientHeight)) {
      this.getOlderLogs();
    }
  }

  showPopup() {
    // this.popupRequest = request;
    this.bsModalRef = this.modalService.show(LogsSettingsComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
    this.bsModalRef.content.parentController = this;
  }

  getLogStyle(log: string) :string {
    if (log.indexOf('[DEBUG') == 0) {
      return 'darkslategrey';
    } else if (log.indexOf('[LOW_INFO') == 0) {
      return 'teal';
    } else if (log.indexOf('[INFO') == 0) {
      return 'cyan';
    } else if (log.indexOf('[WARNING') == 0) {
      return 'darkorange';
    } else if (log.indexOf('[ERROR') == 0) {
      return 'red';
    } else if (log.indexOf('[SUCCESS') == 0) {
      return 'chartreuse';
    } else {
      return 'white';
    }
  }

  getFontSize() :string {
    return this.logsFontSize;
  }

  setFontSize(size: string): void {
    this.logsFontSize = size;
  }


  onLoginSubmit() {
    const user = {
      username: this.username,
      password: this.password,
      rememberme: this.rememberme
    }

    console.log('>>>>> on login submit');

    this.authService.authenticateUser(user).subscribe(data => {
      if(data.success){
        this.authService.storeUserData(data.token, data.user, data.expiresIn);
        this.flashMessage.show('You are now logged in', {
          cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else {
        this.flashMessage.show(data.msg, {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      }
      this.router.navigate(['home']);
    });
  }

}


@Component({
  selector: 'modal-content',
  styleUrls: ['./home.component.css'],
  template: `
    <div class="modal-header">
      <h5 class="modal-title pull-left modal-header-custom">Log filters</h5>
    </div>
    <div class="modal-body modal-body-custom">
      <div class="modal-content-center">
        <textarea class="form-control" id="logFiltersAreaText" rows="5" [(ngModel)]="filters"></textarea>
      </div>
      <div modal-input-section class="modal-content-center mt-3">
          <button type="button" class="btn btn-default mr-2" onclick="this.blur();" (click)="onPopupCancel()">Cancel</button>
          <button type="button" class="btn btn-success" onclick="this.blur();"  (click)="onPopupConfirm()">Save</button>
      </div>
    </div>
  `
})

export class LogsSettingsComponent implements OnInit {

  parentController : HomeComponent;
  popupRequest: string;
  filters: string;

  constructor(
    public bsModalRef: BsModalRef,
    private restService:RestService,
    private flashMessage: FlashMessagesService
  ) {}

  ngOnInit() {
    this.loadFilters();
  }

  onPopupConfirm() : void {
    this.saveFilters();
    this.bsModalRef.hide();
  }

  onPopupCancel() : void {
    this.bsModalRef.hide();
  }

  loadFilters() {

    this.restService.getJson('log_filters').subscribe(data => {
      //data = this.convertFromBufferToJson(data);
      console.log('data filters: ', data);
      console.log('>>> err code: ', data.errorCode);
      if(data.errorCode){

        this.flashMessage.show('Loading log filters failed: ' + data.result, {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else {
        var jsonObject = JSON.parse(data.json);
        var logFilters = jsonObject['filters'];
        this.filters = logFilters.join('\n');
      }
    });
  }

  saveFilters() {
    var filtersObject = {'filters' : this.filters.split('\n')};

    this.restService.saveJson('log_filters', filtersObject).subscribe(data => {
      //data = this.convertFromBufferToJson(data);
      console.log('data: ', data);
      console.log('>>> err code: ', data.errorCode);
      if(data.errorCode){

        this.flashMessage.show('Saving filters list failed: ' + data.result, {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else {
        this.flashMessage.show('Filters saved successfully', {
          cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      }
    });
  }
}
