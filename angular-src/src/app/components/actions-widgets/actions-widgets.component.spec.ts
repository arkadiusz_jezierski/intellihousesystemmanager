import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionsWidgetsComponent } from './actions-widgets.component';

describe('ActionsWidgetsComponent', () => {
  let component: ActionsWidgetsComponent;
  let fixture: ComponentFixture<ActionsWidgetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionsWidgetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionsWidgetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
