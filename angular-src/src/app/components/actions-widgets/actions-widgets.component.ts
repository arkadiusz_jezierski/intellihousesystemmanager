import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ActionsWidgetsService } from '../../services/actions-widgets.service';
import { ActionGroup } from '../../models/action-group';
import { FlashMessagesService } from 'angular2-flash-messages';
import {
  URL_PARAM_KEY, URL_PARAM_EDIT, WIDGET_TYPES, WIDGET_DETAILS_SLIDER_MAX, WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC,
  WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR, WIDGET_TYPE_SLIDER, WIDGET_TYPE_BUTTONS_1, WIDGET_TYPE_BUTTONS_2,
  WIDGET_TYPE_BUTTONS_3, WIDGET_TYPE_BUTTONS_4, WIDGET_DETAILS_BUTTON_NAME, WIDGET_DETAILS_BUTTON_COLOR, WIDGET_DETAILS_BUTTON_SETTINGS
} from "../../models/consts";
import { ActionWidget } from "../../models/action-widget";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { BsModalService } from 'ngx-bootstrap/modal';
import { WidgetActionSelectComponent } from '../widget-action-select/widget-action-select.component';
import { WidgetAction } from "../../models/widget-action";
import { WidgetActionSetup } from "../../models/widget-action-setup";
import { RequestedFields } from "../../models/widget-action-setup";
import { RestService } from '../../services/rest.service';
import { Device } from '../../models/device';
import { DeviceManagerService } from '../../services/device-manager.service';
import { Utils } from "../../utils/utils";

@Component({
  selector: 'app-actions-widget',
  templateUrl: './actions-widgets.component.html',
  styleUrls: ['./actions-widgets.component.css']
})
export class ActionsWidgetsComponent implements OnInit {

  bsModalRef: BsModalRef;
  groupKey : number;
	editMode : boolean;
	actionGroup : ActionGroup;
  groupName : string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  	public actionWidgetsService : ActionsWidgetsService,
  	public flashMessage: FlashMessagesService,
    private modalService: BsModalService,
    private restService:RestService,
    private deviceManager:DeviceManagerService
  ) { }

  ngOnInit() {
    this.deviceManager.loadDevices();
  	this.route.params.subscribe(params => {
      this.groupKey = params[URL_PARAM_KEY];
			this.editMode = (params[URL_PARAM_EDIT] == 1);
   
      console.log('>>>> this groupKey: ', this.groupKey);
      console.log('>>>> this editMode: ', this.editMode);

      this.loadWidgets();


    });
  }

  loadWidgets() {
    this.actionWidgetsService.getActionWidgetsGroup(this.groupKey, (val => {
      if (!val) {
        let navigationExtras: NavigationExtras = {
          queryParams: {
            "cssClass": "alert-danger",
            "message": "Loading action widget group failed"
          }
        };
        this.router.navigate(['/widgetsgroupslist'], navigationExtras);
      }
      this.actionGroup = val;
      this.groupName = val.name;
    }));
  }

  onSave() {
    this.actionGroup.name = this.groupName;
  	this.actionWidgetsService.updateActionGroup(this.actionGroup, (callback => {
      if (callback['success'] === false){
        this.flashMessage.show('Creating action widget group failed: ' + callback['msg'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
        
      } else if (callback['success'] === true) {
        this.flashMessage.show('Action widget group saved', {
          cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      }
  	}));
  }

  onCancel() {
  	this.router.navigate(['/widgetsgroupslist']);
  }

  onActivate(widget : ActionWidget) {
    widget.active = true;
    this.actionWidgetsService.updateWidget(widget, (callback => {
      if (callback['success'] === false){
        this.flashMessage.show('Updating action widget failed: ' + callback['msg'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
        widget.active = false;

      } else if (callback['success'] === true) {
        this.flashMessage.show('Action widget updated', {
          cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      }
    }));
  }

  onInactivate(widget : ActionWidget) {
    widget.active = false;
    this.actionWidgetsService.updateWidget(widget, (callback => {
      if (callback['success'] === false){
        this.flashMessage.show('Updating action widget failed: ' + callback['msg'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
        widget.active = true;

      } else if (callback['success'] === true) {
        this.flashMessage.show('Action widget updated', {
          cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      }
    }));
  }

  onDelete(widget : ActionWidget) {
    this.actionWidgetsService.deleteActionsWidget(widget.key, (callback => {
      if (callback['success'] === false){
        this.flashMessage.show('Delete widget failed: ' + callback['msg'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else if (callback['success'] === true) {
        this.flashMessage.show('Widget deleted', {
          cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});

        this.loadWidgets();
      }
    }));
  }

  showWidget(widget : ActionWidget) {
    return this.editMode || widget.active;
  }

  addWidget() {
    this.showNewWidgetPopup();
  }

  onNameClicked(event) {
    this.showRenameWidgetPopup(event);
  }

  onActionSetup(event) {
    // this.showWidgetActionPopup(event);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "actionsetup": JSON.stringify(event),
        "groupkey": this.groupKey
      }
    };
    this.router.navigate(['/widgetactionslist'], navigationExtras);
  }

  onAction(event) {
    console.log('>>> event: ', event);
    var device = this.deviceManager.getDevice(event.devCategory, event.devAddress);
    this.restService.setDeviceParameter(
      device, event.devCommand, event.devCommandValue).subscribe(data => {
        if(data.errorCode){
          console.log('>> error: ', data.result);
        } else {
          console.log('>> success');
          if(!device.parameters) {
            device.parameters = {};
          }
          device.parameters[event.devCommand] = event.devCommandValue;
        }
      }
    );
  }

  showNewWidgetPopup() {
    // this.popupRequest = request;
    this.bsModalRef = this.modalService.show(WidgetPopupComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
    this.bsModalRef.content.parentController = this;
    this.bsModalRef.content.popupType = 'new_widget';
  }

  showEditWidgetPopup(widget : ActionWidget) {
    this.bsModalRef = this.modalService.show(WidgetPopupComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
    this.bsModalRef.content.parentController = this;
    this.bsModalRef.content.popupType = 'edit_widget';
    this.bsModalRef.content.widget = widget;
    this.bsModalRef.content.newWidgetName = widget.name;
    this.bsModalRef.content.newWidgetType = widget.type;
    if (widget.details) {
      if (widget.type == WIDGET_TYPE_SLIDER) {
        var details = JSON.parse(widget.details);
        this.bsModalRef.content.maxSliderValue = details[WIDGET_DETAILS_SLIDER_MAX];
        this.bsModalRef.content.showSliderGraphic = details[WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC];
        this.bsModalRef.content.sliderGraphicColor = details[WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR];
      }
      if (widget.type == WIDGET_TYPE_BUTTONS_1 || widget.type == WIDGET_TYPE_BUTTONS_2 
          || widget.type == WIDGET_TYPE_BUTTONS_3 || widget.type == WIDGET_TYPE_BUTTONS_4) {
        var buttonSettings = JSON.parse(widget.details);
        this.bsModalRef.content.buttonSettings = buttonSettings[WIDGET_DETAILS_BUTTON_SETTINGS];
      }
    }
    
  }

  showDeleteWidgetPopup(widget : ActionWidget) {
    // this.popupRequest = request;
    this.bsModalRef = this.modalService.show(WidgetPopupComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
    this.bsModalRef.content.parentController = this;
    this.bsModalRef.content.popupType = 'delete_widget';
    this.bsModalRef.content.widget = widget;
  }

  showRenameWidgetPopup(widget : ActionWidget) {
    // this.popupRequest = request;
    this.bsModalRef = this.modalService.show(WidgetPopupComponent);
    this.bsModalRef.content.closeBtnName = 'Close';
    this.bsModalRef.content.parentController = this;
    this.bsModalRef.content.popupType = 'rename_widget';
    this.bsModalRef.content.widget = widget;
    this.bsModalRef.content.newWidgetName = widget.name;
  }

}

@Component({
  selector: 'modal-content',
  styleUrls: ['./actions-widgets.component.css'],
  template: `
      <div *ngIf="popupType == 'new_widget' || popupType == 'edit_widget'">
        <div class="modal-header">
          <h4 class="modal-title pull-left modal-header-custom">New widget</h4>
        </div>
        <div class="modal-body">
          <div class="">
              Select widget type:
          </div>
     
          <div>
            <select class="form-control half-size" id="widgetType" [(ngModel)]="newWidgetType">
              <option *ngFor="let type of widgetTypes" [ngValue]="type">
                {{type}}
              </option>
            </select>
          </div>
          <div class="mt-3">
            Widget name:
          </div>
          <div>
            <input type="text" [(ngModel)]="newWidgetName" class="form-control half-size" id="widgetName">
          </div>
          
          <div *ngIf="newWidgetType == 'slider'">
            <div class="mt-3">
              Max slider value:
            </div>
            <div>
              <input type="number" [(ngModel)]="maxSliderValue" class="form-control half-size" id="sliderMaxValue">
            </div>
            <div class="mt-3">
              Show slider graphic:
            </div>
            <div>
              <ui-switch
                size="small"
                switchColor="#fcfcfc"
                color="rgb(35,255,150);"
                defaultBgColor="rgb(255,80,80);"
                defaultBoColor="black"
                [(ngModel)]="showSliderGraphic"></ui-switch>
            </div>
            <div class="mt-3">
              Set graphic color (RRGGBB):
            </div>
            <div>
              <input type="text" [(ngModel)]="sliderGraphicColor" class="form-control half-size" [style.background]="getBackgroundColor()" id="sliderGraphicColor">
            </div>
          </div>
          
          <div *ngIf="newWidgetType == '1-button' || newWidgetType == '2-buttons' || newWidgetType == '3-buttons' || newWidgetType == '4-buttons'">
            <div *ngFor="let buttonSettings of getButtonIds(); let i = index;" >
              <div class="mt-3">
                Button {{i}} settings:
              </div>
              <div class="switch button-settings">
             
                <input type="text" placeholder="Button name" [(ngModel)]="buttonSettings.buttonName" class="form-control button-name mr-2">
             
                <ui-switch
                size="small"
                switchColor="#fcfcfc"
                color="red"
                defaultBgColor="#555555"
                defaultBoColor="black"
                [(ngModel)]="buttonSettings.buttonColor"></ui-switch>
       
              </div>
            </div>
              
          </div>
          
          <div modal-input-section class="modal-content-center mt-5">
              <button type="button" class="btn btn-default mr-2" onclick="this.blur();" (click)="onPopupCancel()">Cancel</button>
              <button type="button" *ngIf="popupType == 'new_widget'" class="btn btn-success" onclick="this.blur();"  (click)="onSaveNewWidget()">Save</button>
              <button type="button" *ngIf="popupType == 'edit_widget'" class="btn btn-success" onclick="this.blur();"  (click)="onSaveWidget()">Save</button>
          </div>
        </div>
      </div>
      
      <div *ngIf="popupType == 'delete_widget'">
        <div class="modal-header">
          <h4 class="modal-title pull-left modal-header-custom">WARNING</h4>
        </div>
        <div class="modal-body modal-content-center">
            <div>
                Do you really want to remove the widget?
            </div>
            <div modal-input-section class="modal-content-center mt-3">
                <button type="button" class="btn btn-danger wide-button mr-2" onclick="this.blur();" (click)="onPopupCancel()">No</button>
                <button type="button" class="btn btn-success" onclick="this.blur();"  (click)="onDeleteWidget()">Yes</button>
            </div>
        </div>
      </div>

      <div *ngIf="popupType == 'rename_widget'">
        <div class="modal-header">
          <h4 class="modal-title pull-left modal-header-custom">Set widget name</h4>
        </div>
        <div class="modal-body">
          <div class="mt-3">
            Widget name:
          </div>
          <div>
            <input type="text" [(ngModel)]="newWidgetName" class="form-control half-size" id="widgetName">
          </div>

          <div modal-input-section class="modal-content-center mt-5">
            <button type="button" class="btn btn-default mr-2" onclick="this.blur();" (click)="onPopupCancel()">Cancel</button>
            <button type="button" class="btn btn-success" onclick="this.blur();"  (click)="onSaveWidget()">Save</button>
          </div>
        </div>
      </div>
    
  `
})


export class WidgetPopupComponent implements OnInit {

  parentController : ActionsWidgetsComponent;
  widget : ActionWidget;
  popupType : string;
  newWidgetType : string;
  newWidgetName : string;
  widgetTypes : string[];
  setup : WidgetActionSetup;
  maxSliderValue : number;
  showSliderGraphic : boolean;
  sliderGraphicColor : string;
  buttonSettings;
 

  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit() {
    this.widgetTypes = WIDGET_TYPES;
    if (this.buttonSettings == undefined) {
      this.buttonSettings = [];
 
      for (var i = 0; i < 4; i++) {
        var obj = {};
        obj[WIDGET_DETAILS_BUTTON_NAME] = '' + (i + 1);
        obj[WIDGET_DETAILS_BUTTON_COLOR] = false;
        this.buttonSettings.push(obj);
      }
    }
    
  }

  getButtonIds() {
    var tab = [];
    if (this.buttonSettings && this.buttonSettings.length > 0) {
      var buttonsQnty = 0;
      if (this.newWidgetType == WIDGET_TYPE_BUTTONS_1) {
        buttonsQnty = 1;
      } else if (this.newWidgetType == WIDGET_TYPE_BUTTONS_2) {
        buttonsQnty = 2;
      } else if (this.newWidgetType == WIDGET_TYPE_BUTTONS_3) {
        buttonsQnty = 3;
      } else if (this.newWidgetType == WIDGET_TYPE_BUTTONS_4) {
        buttonsQnty = 4;
      } else {
        buttonsQnty = 0;
      }

      for (var i = 0; i < buttonsQnty; i++) {
        tab.push(this.buttonSettings[i]);
      }
    }
    
    return tab;
  }

  onSaveNewWidget() : void {
    // this.parentController.deleteActionWidgetsGroup(this.actionGroupKey);
    var details = this.generateWidgetDetails();
    
    this.parentController.actionWidgetsService.createNewWidget(this.parentController.groupKey, this.newWidgetType, true, this.newWidgetName, details, (callback => {
      if (callback['success'] === false){
        this.parentController.flashMessage.show('Creating widget failed: ' + callback['msg'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else if (callback['success'] === true) {
        this.parentController.flashMessage.show('New widget created', {
          cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});

        this.parentController.loadWidgets();
      }
    }));
    this.bsModalRef.hide();
  }

  onSaveWidget() : void {
    this.widget.name = this.newWidgetName;
    if (this.popupType == 'edit_widget') {
      this.widget.type = this.newWidgetType;
      this.widget.details = this.generateWidgetDetails();
    }
    // this.parentController.deleteActionWidgetsGroup(this.actionGroupKey);
    this.parentController.actionWidgetsService.updateWidget(this.widget, (callback => {
      if (callback['success'] === false){
        this.parentController.flashMessage.show('Updating widget failed: ' + callback['msg'], {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else if (callback['success'] === true) {
        this.parentController.flashMessage.show('Widget updated', {
          cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});

        this.parentController.loadWidgets();
      }
    }));
    this.bsModalRef.hide();
  }

  onDeleteWidget() : void {
    this.parentController.onDelete(this.widget);
    this.bsModalRef.hide();
  }

  onPopupCancel() : void {
    this.bsModalRef.hide();
  }

  getBackgroundColor() {
    if (this.sliderGraphicColor && this.sliderGraphicColor.length == 6) {
      return new Utils().hexColorToRgbCss(this.sliderGraphicColor);
    } else {
      return new Utils().hexColorToRgbCss('000000');
    }
    
  }

  generateWidgetDetails() {
    var detailsObj = {};
    if (this.maxSliderValue != undefined) {
      detailsObj[WIDGET_DETAILS_SLIDER_MAX] = this.maxSliderValue;
    }
    if (this.showSliderGraphic != undefined) {
      detailsObj[WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC] = this.showSliderGraphic;
    } else if (this.newWidgetType == WIDGET_TYPE_SLIDER) {
      detailsObj[WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC] = false;
    }
    if (this.sliderGraphicColor != undefined) {
      detailsObj[WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR] = this.sliderGraphicColor;
    }

    if (this.newWidgetType == WIDGET_TYPE_BUTTONS_1 || this.newWidgetType == WIDGET_TYPE_BUTTONS_2 
        ||this.newWidgetType == WIDGET_TYPE_BUTTONS_3 || this.newWidgetType == WIDGET_TYPE_BUTTONS_4) {
      detailsObj[WIDGET_DETAILS_BUTTON_SETTINGS] = this.buttonSettings;
    }

    if (Object.keys(detailsObj).length > 0) {
      return JSON.stringify(detailsObj);
    } else {
      return '';
    }
  }
}
