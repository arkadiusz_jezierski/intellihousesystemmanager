import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionWidgetRgbSliderComponent } from './action-widget-rgb-slider.component';

describe('ActionWidgetRgbSliderComponent', () => {
  let component: ActionWidgetRgbSliderComponent;
  let fixture: ComponentFixture<ActionWidgetRgbSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionWidgetRgbSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionWidgetRgbSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
