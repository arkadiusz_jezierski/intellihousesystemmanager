import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionWidgetButtonsComponent } from './action-widget-buttons.component';

describe('ActionWidgetButtonsComponent', () => {
  let component: ActionWidgetButtonsComponent;
  let fixture: ComponentFixture<ActionWidgetButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionWidgetButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionWidgetButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
