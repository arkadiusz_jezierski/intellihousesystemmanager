import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services/rest.service';
import { ConfigService } from '../../services/config.service';
import { DeviceManagerService } from '../../services/device-manager.service';
import { Device } from '../../models/device';
import { DeviceConsts } from '../../utils/deviceConsts';
import { ConfigConsts } from '../../utils/configConsts';
import { ActivatedRoute } from '@angular/router';
import { Utils } from "../../utils/utils";
import { Router } from '@angular/router';

@Component({
  selector: 'app-sockets',
  templateUrl: './sockets.component.html',
  styleUrls: ['./sockets.component.css']
})



export class SocketsComponent implements OnInit {

  currentDevices: string[] = [
    DeviceConsts.KITCHEN_SOCKET_01,
    DeviceConsts.KITCHEN_SOCKET_02,
    DeviceConsts.KITCHEN_SOCKET_03,
    DeviceConsts.KITCHEN_SOCKET_04,
    DeviceConsts.KITCHEN_SOCKET_05,
    DeviceConsts.KITCHEN_SOCKET_06,
    DeviceConsts.LIVINGROOM_SOCKET_01
  ];

  devicesMap: Object;

  constructor(
    private restService:RestService,
    private deviceManager: DeviceManagerService,
    private configService: ConfigService,
    private route: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit() {
    this.deviceManager.loadDevices(() => {this.loadDevices();});
  }


  loadDevices(): void {
    console.log('Loading devices config');

    this.configService.getDevices(this.currentDevices).subscribe(data => {
      console.log('data: ', data);
      if (data.success) {
        console.log('GetDevices success, devices list: ', data.devices);
        this.devicesMap = new Object();

        for (var dev of data.devices) {
          console.log('dev:' ,dev);
          this.devicesMap[dev.key] = this.deviceManager.getDevice(dev.category, dev.address);
        }
        console.log('>>>> devices map: ', this.devicesMap);
      } else {
        console.log('GetDevices error: ', data.msg);
      }
    });
  }

  setSocketPower(status: Boolean, key: string): void {
    console.log('>>> set power ' + (status ? 'ON' : 'OFF') + ' for socket: ' + key);
    this.setDeviceParameter(this.devicesMap[key], DeviceConsts.PARAM_STATE, status? 1 : 0);
  }

  setDeviceParameter(dev: Device, param: string, val: number) : void {
    this.restService.setDeviceParameter(dev, param, val).subscribe(data => {
      if(data.errorCode){
        console.log('Setting device parameter failed: ' + data.result);
      } else {
        console.log('Device parameter set successfully!');
      }
    });
  }
}
