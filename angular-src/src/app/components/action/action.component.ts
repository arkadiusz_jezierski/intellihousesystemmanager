import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OperationManagerService } from '../../services/operation-manager.service';
import { Operation, Action } from '../../models/operation';
import { Device } from '../../models/device';
import { DeviceManagerService } from '../../services/device-manager.service';
import { BuilderService } from '../../services/builder.service';
import { Router } from '@angular/router';
import { ValidateService } from '../../services/validate.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import {SRC_PAGE_ACT_EDIT} from "../../models/consts";

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.css']
})

export class ActionComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private operationManager: OperationManagerService,
    private router:Router,
    private deviceManager:DeviceManagerService,
    private builder:BuilderService,
    private validator: ValidateService,
    private flashMessage:FlashMessagesService
  ) { }

  actionId: number;
  operation: Operation;
  action: Object;
  types: string[];
  private sub: any;


  ngOnInit() {

    this.loadActionTypes();

    this.sub = this.route.params.subscribe(params => {
      this.actionId = params['id'];
      this.operation = this.operationManager.tmpOperation;

      if (this.actionId != 0) {
        var tmpAction: Action = this.operationManager.getOperationActionById(this.operation, this.actionId);
        this.action = tmpAction.toJson();
      } else {
        this.action = new Object();
        this.action['number'] = '+48';
      }


    });
  }

  loadActionTypes() {
    this.types = new Array<string>();
    this.types.push('device');
    this.types.push('gsmModem');
    this.types.push('rest');
  }

  getDevicesForSelectedCategory() :Device[] {
    if (this.action['category']) {
      var devs :Device[] = this.deviceManager.devicesList.filter((item)=>item.category == this.action['category']);
      console.log('>>>> devices: ', devs);
      return devs;
    }
    return new Array<Device>();
  }

  getParamsForSelectedCategory() :string[] {
    var params :string[] = new Array<string>();
    if (this.action['category']) {
      return this.deviceManager.getCategoryById(this.action['category']).commands;
    }

    return params;
  }

  onActionSave() {

    var res: Object = new Object();

    if (this.validator.validateAction(this.action, res)) {

      if (this.actionId != 0) {
        this.operationManager.setOperationActionById(
          this.operation,
          this.actionId,
          this.builder.createAction(
            this.action,
            this.deviceManager
          )
        );
      } else {
        var tmpAction :Action = this.builder.createAction(this.action, this.deviceManager);
        this.operation.actions.push(tmpAction);
      }

      this.router.navigate(['/operation', this.operation.id, SRC_PAGE_ACT_EDIT]);

    } else {
      this.flashMessage.show(res['error'], {
        cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
        timeout: 5000});
    }


  }

  onCancel() {
    this.router.navigate(['/operation', this.operation.id, SRC_PAGE_ACT_EDIT]);
  }

}
