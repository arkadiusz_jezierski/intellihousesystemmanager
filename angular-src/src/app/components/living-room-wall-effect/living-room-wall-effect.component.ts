import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services/rest.service';
import { ConfigService } from '../../services/config.service';
import { DeviceManagerService } from '../../services/device-manager.service';
import { Device } from '../../models/device';
import { DeviceConsts } from '../../utils/deviceConsts';
import { ConfigConsts } from '../../utils/configConsts';
import { ActivatedRoute } from '@angular/router';
import { Utils } from "../../utils/utils";
import { Router } from '@angular/router';


@Component({
  selector: 'app-living-room-wall-effect',
  templateUrl: './living-room-wall-effect.component.html',
  styleUrls: ['./living-room-wall-effect.component.css']
})
export class LivingRoomWallEffectComponent implements OnInit {

  currentDevices: string[] = [
    DeviceConsts.LIVINGROOM_WALL_EFFECT_TOP,
    DeviceConsts.LIVINGROOM_WALL_EFFECT_MIDDLE,
    DeviceConsts.LIVINGROOM_WALL_EFFECT_BOTTOM
  ];

  devicesMap: Object;
  utils: Utils;
  speed: number;



  constructor(
    private restService:RestService,
    private deviceManager: DeviceManagerService,
    private configService: ConfigService,
    private route: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit() {

    console.log('>>> onInit');
    this.utils = new Utils();
    this.deviceManager.loadDevices(() => {this.loadDevices();});
  }

  loadDevices(): void {
    console.log('Loading devices config');

    this.configService.getDevices(this.currentDevices).subscribe(data => {
      console.log('data: ', data);
      if (data.success) {
        console.log('GetDevices success, devices list: ', data.devices);
        this.devicesMap = new Object();

        for (var dev of data.devices) {
          console.log('dev:' ,dev);
          this.devicesMap[dev.key] = new Object();
          this.devicesMap[dev.key].device = this.deviceManager.getDevice(dev.category, dev.address);
          this.devicesMap[dev.key].red = 0;
          this.devicesMap[dev.key].green = 0;
          this.devicesMap[dev.key].blue = 0;
          this.devicesMap[dev.key].mode = 0;
          this.devicesMap[dev.key].speed = 0;
          this.devicesMap[dev.key].selected = false;
        }

        this.loadDevicesParameters();

        console.log('>>>> devices map: ', this.devicesMap);

      } else {
        console.log('GetDevices error: ', data.msg);
      }


    });
  }

  loadDevicesParameters(): void {
    console.log('Loading devices parameters');
    for (var devKey of this.currentDevices) {

      this.loadDeviceParameters(devKey, (key, data) => {

        for (var param = 0; param < data.parameters.length; param++) {
          if (!data.parameters[param].defined) {
            console.log('Device parameter ' + param + ' is undefined');
          } else {
            if (data.parameters[param]['parameter'] == DeviceConsts.PARAM_RED) {
              this.devicesMap[key].red = data.parameters[param]['value'];
            }
            if (data.parameters[param]['parameter'] == DeviceConsts.PARAM_GREEN) {
              this.devicesMap[key].green = data.parameters[param]['value'];
            }
            if (data.parameters[param]['parameter'] == DeviceConsts.PARAM_BLUE) {
              this.devicesMap[key].blue = data.parameters[param]['value'];
            }
            if (data.parameters[param]['parameter'] == DeviceConsts.PARAM_SPEED) {
              this.devicesMap[key].speed = data.parameters[param]['value'];
              this.speed = data.parameters[param]['value'];
            }
            if (data.parameters[param]['parameter'] == DeviceConsts.PARAM_MODE) {
              this.devicesMap[key].mode = data.parameters[param]['value'];
            }
          }
        }

      });

    }
  }

  loadDeviceParameters(deviceKey: string, callback: (key: string, val) => void) :void {
    this.restService.getDeviceParameters(this.devicesMap[deviceKey].device).subscribe(data => {
      if(data.errorCode){
        console.log('Loading device parameters failed: ' + data.result);
      } else {
        callback(deviceKey, data);
      }
    });
  }

  getStyle(id) :string {
    var retVal = 'rgb(';
    if (this.devicesMap && this.devicesMap[id]) {
      retVal += ((this.devicesMap[id].red >> 4) & 0xff);
      retVal += ',';
      retVal += ((this.devicesMap[id].green >> 4) & 0xff);
      retVal += ',';
      retVal += ((this.devicesMap[id].blue >> 4) & 0xff);
    } else {
      retVal += '0,0,0';
    }

    retVal += ')';
    return retVal;
  }

  loadEffectConfig(event) :void {
    var setName = event['target'].attributes['id'].value;
    console.log('Loading effect set ' + setName);
    this.clearDeviceSelection();
    this.configService.getDevicesConfigs(setName).subscribe(data => {
      console.log('data: ', data);
      if (data.success) {
        console.log('config: ', data.configs);
        this.setDeviceParameter(this.devicesMap[this.currentDevices[0]].device, DeviceConsts.PARAM_MODE, DeviceConsts.RGB_MODE_MANUAL);

        for (var config of data.configs) {

          var key = this.getDeviceKey(config.address, config.category);
          this.setDeviceParameter(this.devicesMap[key].device, config.parameter, config.value);
          this.updateCurrentDevices(key, config.value, DeviceConsts.RGB_MODE_MANUAL);

          console.log('Loading config for device ' + key + ', parameter ' + config.parameter + ' from config = ' + config.value);
        }

      } else {
        console.log('GetDevicesConfigs error: ', data.msg);
      }

    });
  }

  saveEffectConfig(event) :void {
    var setName = event['target'].attributes['id'].value;
    console.log('Saving effect set ' + setName);

    for (var devKey of this.currentDevices) {
      var rgb = this.utils.getRgb(this.devicesMap[devKey].red, this.devicesMap[devKey].green, this.devicesMap[devKey].blue);
      console.log('Saving rgb value ' + rgb + ' for device ' + devKey);
      this.configService.addDeviceConfig(setName, this.devicesMap[devKey].device, DeviceConsts.PARAM_RGB, rgb).subscribe(data=>{
        console.log('Saving config success');
      });
    }
  }

  getDeviceKey(address: string, category: string) :string{
    for (var devKey of this.currentDevices) {
      if (this.devicesMap[devKey].device.address == address && this.devicesMap[devKey].device.category == category) {
        return devKey;
      }
    }
  }

  setDeviceParameter(dev: Device, param: string, val: number) : void {
    this.restService.setDeviceParameter(dev, param, val).subscribe(data => {
      if(data.errorCode){
        console.log('Setting device parameter failed: ' + data.result);
      } else {
        console.log('Device parameter set successfully! ');
      }
    });
  }

  setDevicesMode(mode :number) {
    console.log('Set effect in mode ' + mode);
    this.clearDeviceSelection();
    var set = false;
    for (var devKey of this.currentDevices) {
      if (!set) {
        this.setDeviceParameter(this.devicesMap[devKey].device, DeviceConsts.PARAM_MODE, mode);
        set = true;
      }
      this.updateCurrentDevicesMode(devKey, mode);
    }
  }

  updateCurrentDevices(devKey :string, rgb :number, mode :number) :void {
    this.devicesMap[devKey].mode = mode;
    this.devicesMap[devKey].red = this.utils.getRed12bit(rgb);
    this.devicesMap[devKey].green = this.utils.getGreen12bit(rgb);
    this.devicesMap[devKey].blue = this.utils.getBlue12bit(rgb);
    console.log('>>>> dev: ' , this.devicesMap[devKey]);
  }

  updateCurrentDevicesMode(devKey :string, mode :number) :void {
    this.devicesMap[devKey].mode = mode;
    console.log('>>>> dev: ' , this.devicesMap[devKey]);
  }

  increaseSpeed() :void {
    console.log('Increasing speed');
    if (this.speed < 3) {
      this.speed++;
      var set = false;
      for (var devKey of this.currentDevices) {
        if (!set) {
          this.setDeviceParameter(this.devicesMap[devKey].device, DeviceConsts.PARAM_SPEED, this.speed);
          set = true;
        }
        this.devicesMap[devKey].speed = this.speed;
      }
    }
  }

  decreaseSpeed() :void {
    console.log('Decreasing speed');
    if (this.speed > 0) {
      this.speed--;
      var set = false;
      for (var devKey of this.currentDevices) {
        if (!set) {
          this.setDeviceParameter(this.devicesMap[devKey].device, DeviceConsts.PARAM_SPEED, this.speed);
          set = true;
        }
        this.devicesMap[devKey].speed = this.speed;
      }
    }
  }

  goToColorPickerPage(event) :void {
    var devKey = event['target'].attributes['id'].value;
    var dev = this.devicesMap[devKey].device;
    var addresses :any[] = new Array();

    if (this.devicesMap[devKey].mode != DeviceConsts.RGB_MODE_MANUAL) {
      this.setDevicesMode(DeviceConsts.RGB_MODE_MANUAL);
    }

    if (!this.devicesMap[devKey].selected) {
      this.clearDeviceSelection();
      addresses.push(dev.address);
    } else {
      for (var key of this.currentDevices) {
        if (this.devicesMap[key].selected) {
          addresses.push(this.devicesMap[key].device.address);
        }
      }
    }

    this.router.navigate(['/colorpicker', dev.category, addresses.join(),
      this.devicesMap[devKey].red, this.devicesMap[devKey].green, this.devicesMap[devKey].blue, 'livingroomwalleffect']);
  }

  isManualModeOn() :boolean {
    return (this.devicesMap
      && this.devicesMap[this.currentDevices[0]].mode
      && this.devicesMap[this.currentDevices[0]].mode == DeviceConsts.RGB_MODE_MANUAL
    );
  }

  setManualMode() {
    console.log('Switch to manual mode');
    this.clearDeviceSelection();
    if (this.devicesMap[this.currentDevices[0]].mode == DeviceConsts.RGB_MODE_MANUAL) {
      return;
    }

    var set = false;
    for (var devKey of this.currentDevices) {
      if (!set) {
        this.setDeviceParameter(this.devicesMap[devKey].device, DeviceConsts.PARAM_MODE, DeviceConsts.RGB_MODE_MANUAL);
        set = true;
      }

      this.updateCurrentDevicesMode(devKey, DeviceConsts.RGB_MODE_MANUAL);

    }


  }

  getSelected(id) :boolean{
    return (this.devicesMap && this.devicesMap[id].selected);
  }

  selectDevice(event) {
    var id = event['target'].attributes['id'].value;
    if (this.devicesMap[id].selected) {
      this.devicesMap[id].selected = false;
    } else {
      this.devicesMap[id].selected = true;
    }
  }

  clearDeviceSelection() {
    for (var key of this.currentDevices) {
      this.devicesMap[key].selected = false;
    }
  }

}
