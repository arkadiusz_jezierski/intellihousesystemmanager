import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { OperationManagerService } from '../../services/operation-manager.service';
import { ValidateService } from '../../services/validate.service';
import { Operation, Condition, Action } from '../../models/operation';
import { FlashMessagesService } from 'angular2-flash-messages';
import { SRC_PAGE_ACT_EDIT, SRC_PAGE_COND_EDIT, URL_PARAM_ID, URL_PARAM_SRC } from "../../models/consts";

@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.css']
})
export class OperationComponent implements OnInit {

  constructor(
    private flashMessage:FlashMessagesService,
    private route: ActivatedRoute,
    private router: Router,
    private operationManager: OperationManagerService,
    private validator: ValidateService
  ) { }

  operationId: number;
  operation: Operation;
  private sub: any;


  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.operationId = params[URL_PARAM_ID];
      var sourcePage = params[URL_PARAM_SRC];

      if (this.operationId == 0) {
        this.operationManager.createOperation('regular');
      } else if (this.operationId == 1) {

        this.operationManager.createOperation('delayed');
      } else if (sourcePage != SRC_PAGE_ACT_EDIT && sourcePage != SRC_PAGE_COND_EDIT) {
        this.operationManager.cloneOperation(this.operationId);
      }

      this.operation = this.operationManager.tmpOperation;

      console.log('>>>> this operation: ', this.operation);
    });
  }

  onChangeActiveStatus() {
    if (this.operation.active) {
      this.operation.active = false;
    } else {
      this.operation.active = true;
    }
  }

  onSaveOperation() {
    var res: Object = new Object();
    if (this.validator.validateOperation(this.operation, res)) {
      this.operationManager.saveOperation(this.operation);
      this.router.navigate(['/eventmanager']);
    } else {
      this.flashMessage.show(res['error'], {
        cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
        timeout: 5000});
    }

  }

  onCancelOperation() {
    this.operationManager.cancelEditOperation();
    this.router.navigate(['/eventmanager']);
  }

  deleteCondition(conditionId: number) {
    this.operationManager.removeOperationConditionById(this.operation, conditionId);
  }

  cloneCondition(conditionId: number) {
    this.router.navigate(['/condition', conditionId, 1]);
  }

  deleteAction(actionId: number) {
    this.operationManager.removeOperationActionById(this.operation, actionId);
  }

  goToConditionPage(con : Condition) :void {
    this.router.navigate(['/condition', con.id, 0]);
  }

  goToActionPage(act : Action) :void {
    this.router.navigate(['/action', act.id]);
  }

  clearExtLogic(type: string) :void {
    if (type == 'stop') {
      this.operation['stopConditionsExtLogic'] = '';
    } else {
      this.operation.conditionsExtLogic = '';
    }

  }

  restrictLogicPattern(event) :boolean{
    let pattern = /^[A-Z()!|&]$/;
    return pattern.test(event.key);
  }


}
