import { Component, ViewChild, ElementRef, AfterViewInit, Input, Output, HostListener, 
  EventEmitter, OnInit, OnChanges, SimpleChange } from '@angular/core';



@Component({
  selector: 'app-radial-color-picker',
  templateUrl: './radial-color-picker.component.html',
  styleUrls: ['./radial-color-picker.component.css']
})
export class RadialColorPickerComponent implements OnInit, AfterViewInit, OnChanges {

	@ViewChild('canvas')
	canvas: ElementRef<HTMLCanvasElement>;

	@Output()
  color: EventEmitter<string> = new EventEmitter();

  @Output()
  centerRadiusClick: EventEmitter<boolean> = new EventEmitter();

  @Input() size : number;
  @Input() centerRadius : number;
  @Input() redValue : number;
  @Input() greenValue : number;
  @Input() blueValue : number;


  private ctx: CanvasRenderingContext2D;
  private mousedown: boolean = false;

	// xc : number;
	// yc : number;
	// r : number;

  stopColors = ['#F00', '#FF0', '#0F0', '#0FF', '#00F', '#F0F'];

  constructor() { }

  ngOnInit() {
  	// this.xc = this.size / 2;
  	// this.yc = this.size / 2;
  	// this.r = this.xc - this.lineWidth;
  }

  getSize() {
  	if (!this.size) {
  		this.size = 100;
  	}

  	return this.size;
  }

  drawImage() {
    if (!this.ctx) {
      this.ctx = this.canvas.nativeElement.getContext('2d');
    }

    this.ctx.clearRect(0, 0, 1000, 1000);

    var base_image = new Image();
    base_image.src = '../../../assets/img/rainbow.png';
    this.ctx.drawImage(base_image, 0, 0);

    base_image.onload = () => {
      this.ctx.clearRect(0, 0, this.size, this.size);
      this.ctx.drawImage(base_image, 0, 0, this.size, this.size);
    }
  }

  // draw() {
  // 	if (!this.ctx) {
  //   	this.ctx = this.canvas.nativeElement.getContext('2d');
  // 	}
  //
  // 	this.drawMultiRadiantCircle(this.ctx, this.xc , this.yc, this.r, this.stopColors);
  // 	this.drawSelectedColor();
  //
  // }

  // drawMultiRadiantCircle(ctx, xc, yc, r, radientColors) {
  //   var partLength = (2 * Math.PI) / radientColors.length;
  //   var start = 0;
  //   var gradient = null;
  //   var startColor = null,
  //       endColor = null;
  //
  //   for (var i = 0; i < radientColors.length; i++) {
  //       startColor = radientColors[i];
  //       endColor = radientColors[(i + 1) % radientColors.length];
  //
  //       // x start / end of the next arc to draw
  //       var xStart = +xc + Math.cos(start) * r;
  //       var xEnd = +xc + Math.cos(start + partLength) * r;
  //       // y start / end of the next arc to draw
  //       var yStart = +yc + Math.sin(start) * r;
  //       var yEnd = +yc + Math.sin(start + partLength) * r;
  //
  //       ctx.beginPath();
  //
  //       gradient = ctx.createLinearGradient(xStart, yStart, xEnd, yEnd);
  //       gradient.addColorStop(0, startColor);
  //       gradient.addColorStop(1.0, endColor);
  //
  //       ctx.strokeStyle = gradient;
  //       ctx.arc(xc, yc, r, start, start + partLength + 0.05);
  //       ctx.lineWidth = this.lineWidth;
  //       ctx.stroke();
  //       ctx.closePath();
  //
  //       start += partLength;
  //   }
	// }

	// drawSelectedColor() {
	// 	if (!this.ctx) {
  //   	this.ctx = this.canvas.nativeElement.getContext('2d');
  // 	}
  //
	// 	this.ctx.beginPath();
  //   this.ctx.arc(this.xc, this.yc, this.centerRadius, 0, 2 * Math.PI, false);
  //   this.ctx.fillStyle = "rgb("+this.redValue+", "+this.greenValue+", "+this.blueValue+")";
  //   this.ctx.fill();
  //   //this.ctx.lineWidth = 1;
  //   //this.ctx.strokeStyle = '#000';
  //   //this.ctx.stroke();
	// }

  ngAfterViewInit() {
  	this.drawImage();
	}

  touchmove(ev){
    var touch = ev.touches[0];
    var cordX = touch.clientX - (touch.target.offsetParent.offsetLeft + touch.target.offsetParent.offsetParent.offsetLeft);
    var cordY = touch.pageY - touch.target.offsetParent.offsetTop - touch.target.offsetParent.offsetParent.offsetTop;
    this.emitColor(cordX, cordY);
    ev.preventDefault();
		ev.stopPropagation();
	}

	onMouseDown(evt: MouseEvent) {
	  this.mousedown = true;
	  //console.log('>>>onMouseDown evt.offsetX: ' + evt.offsetX + ', evt.offsetY: ' + evt.offsetY);
	  this.emitColor(evt.offsetX, evt.offsetY);
	}

	onMouseMove(evt: MouseEvent) {
	  if (this.mousedown) {
	    this.emitColor(evt.offsetX, evt.offsetY);
	    evt.preventDefault();
	    evt.stopPropagation();
	  }
	}

	@HostListener('window:mouseup', ['$event'])
	onMouseUp(evt: MouseEvent) {
	  this.mousedown = false;
	}

	emitColor(x: number, y: number) {
	  var rgbaColor = this.getColorAtPosition(x, y);
	  if (rgbaColor != '') {
	  	console.log('>>> rgb color: ' + rgbaColor);
	  	this.color.emit(rgbaColor);
	  } else {
	    console.log('>>> noc olor selected');
    }
	  
	}

	getColorAtPosition(x: number, y: number) {
	  const imageData = this.ctx.getImageData(x, y, 1, 1).data;
	  if (imageData[0] > 0 || imageData[1] > 0 || imageData[2] > 0) {
			this.redValue = imageData[0];
		  this.greenValue = imageData[1];
		  this.blueValue = imageData[2];
		  // this.drawSelectedColor();
		  return "{\"red\": " + imageData[0] + ", \"green\" : " + imageData[1] + ", \"blue\" : " + imageData[2] + "}";
	  } else {
	  	return '';
	  }
	  
	}

	ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    console.log('>>> change: ', changes);
    if (changes.redValue) {
    	this.redValue = changes.redValue.currentValue / 0xf;
    }
    if (changes.greenValue) {
    	this.greenValue = changes.greenValue.currentValue / 0xf;
    }
    if (changes.blueValue) {
    	this.blueValue = changes.blueValue.currentValue / 0xf;
    }
    //this.drawSelectedColor();
  }

  getCenterPosition() {
    return '' + ((this.size / 2) - this.centerRadius ) + 'px';
  }

  getCenterSize() {
    return '' + (this.centerRadius * 2) + 'px';
  }

  getCenterBorderRadius() {
    return '' + this.centerRadius + 'px';
  }

  getCenterColor() {
    return 'rgb('+this.redValue+', '+this.greenValue+', '+this.blueValue+')';
    // return 'rgb(123,123,222)';
  }

  getSizeInPx() {
    return '' + this.getSize() + 'px';
  }

  onCenterRadiusClick() {
    this.centerRadiusClick.emit(true);
  }

}
