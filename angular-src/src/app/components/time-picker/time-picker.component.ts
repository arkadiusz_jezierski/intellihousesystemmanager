import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';

@Component({
  selector: 'app-time-picker',
  templateUrl: './time-picker.component.html',
  styleUrls: ['./time-picker.component.css']
})
export class TimePickerComponent implements OnInit {

  seconds: number;
  // @Input()  secondsFromMidnight: number;
  @Input() set secondsFromMidnight(secs: number) {
    this.seconds = secs;
    this.secondsFromMidnightChange.emit(secs);
  }

  get secondsFromMidnight(): number {
    return this.seconds;
  }

  @Output() secondsFromMidnightChange: EventEmitter<number> = new EventEmitter<number>();

  selectedHour: number;
  selectedMinute: number;
  selectedSecond: number;

  constructor() { }

  ngOnInit() {
    console.log('>>>>> seconds: ', this.seconds);
    this.selectedHour = Math.floor(this.seconds / 3600);
    var minutes = this.seconds - (3600 * this.selectedHour);
    this.selectedMinute =  Math.floor(minutes / 60);
    this.selectedSecond = minutes % 60;

  }

  onHourChange(selectedIndex): void {
    console.log(">>>>> onchange: ", selectedIndex);
    this.secondsFromMidnight = selectedIndex * 3600 + this.selectedMinute * 60 + this.selectedSecond;
  }

  onMinuteChange(selectedIndex): void {
    console.log(">>>>> onchange: ", selectedIndex);
    this.secondsFromMidnight = this.selectedHour * 3600 + selectedIndex * 60 + this.selectedSecond;
  }

  onSecondChange(selectedIndex): void {
    console.log(">>>>> onchange: ", selectedIndex);
    this.secondsFromMidnight = this.selectedHour * 3600 + this.selectedMinute * 60 + selectedIndex;
  }

  getHours() :number[] {
    var hours = new Array<number>();
    for (var val = 0; val < 24; val++) {

      hours.push(val);

    }

    return hours;

  }

  getMinutes() :number[] {
    var minutes = new Array<number>();
    for (var val = 0; val < 60; val++) {
      minutes.push(val);

    }

    return minutes;

  }
}
