import {Injectable} from '@angular/core';
import {RestService} from './rest.service';
import {FlashMessagesService} from 'angular2-flash-messages';
import {Action, Condition, DelayedOperation, Operation} from '../models/operation';
import {BuilderService} from './builder.service';
import {DeviceManagerService} from './device-manager.service';
import _ from "lodash";
import {Utils} from '../utils/utils';
import {EStartStopConditionType} from "../models/consts";

@Injectable()
export class OperationManagerService {

  constructor(
    private restService: RestService,
    private flashMessage: FlashMessagesService,
    private builder:BuilderService,
    private deviceManager:DeviceManagerService
  ) { }

  operations: Operation[];
  operationsOrigin: Operation[];
  tmpOperation: Operation;
  changed: boolean;


  loadOperations() {
    if (this.operations) {
      return;
    }

    this.restService.getJson('operation_list').subscribe(data => {
      //data = this.convertFromBufferToJson(data);
      console.log('data: ', data);
      console.log('>>> err code: ', data.errorCode);
      if(data.errorCode){

        this.flashMessage.show('Loading operations list failed: ' + data.result, {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else {
        var jsonObject = JSON.parse(data.json);
        this.loadOperationsList(jsonObject['Operations']);
        this.changed = false;
      }
    });
  }

  loadOperationsList(operationList) {
    if (operationList && operationList.length > 0) {
      this.operations = new Array<Operation>();
      this.operationsOrigin = new Array<Operation>();
    }

    for (var op of operationList) {
      var operation = this.builder.createOperation(op, this.deviceManager);
      this.operations.push(operation);
      // this.operationsOrigin.push(Object.assign(Object.create(operation),operation));
      this.operationsOrigin.push(_.cloneDeep(operation));
    }

  }

  getOperationById(id: number): Operation {
    for (var op of this.operations) {
      if (op.id == id) {
        return op;
      }
    }

    return undefined;
  }

  getOperationConditionById(operation: Operation, condId: number): Condition {
    for (var con of operation['conditions']) {
      if (con.id == condId) {
        return con;
      }
    }
    return undefined;
  }

  getOperationActionById(operation: Operation, actId: number): Action {
    for (var act of operation['actions']) {
      if (act.id == actId) {
        return act;
      }
    }
    return undefined;
  }

  setOperationActionById(operation: Operation, actId: number, action: Action) {
    for (var i = 0; i < operation.actions.length; i++) {
      if (operation.actions[i].id == actId) {
        operation.actions[i] = action;
        break;
      }
    }
  }

  setOperationConditionById(operation: Operation, conId: number, condition: Condition) {
    for (var i = 0; i < operation.conditions.length; i++) {
      if (operation.conditions[i].id == conId) {
        operation.conditions[i] = condition;
        break;
      }
    }
  }

  removeOperationActionById(operation: Operation, actId: number) {
    for (var i = 0; i < operation.actions.length; i++) {
      if (operation.actions[i].id == actId) {
        operation.actions.splice(i, 1);
        break;
      }
    }
  }

  removeOperationConditionById(operation: Operation, condId: number) {
    let condType : EStartStopConditionType;
    for (var i = 0; i < operation.conditions.length; i++) {
      if (operation.conditions[i].id == condId) {
        condType = operation.conditions[i].internalType;
        operation.conditions.splice(i, 1);
        break;
      }
    }

    if (condType == EStartStopConditionType.regular) {
      for (var i = 0; i < operation.conditions.length; i++) {
        operation.conditions[i].extLogicConditionId = new Utils().getLetterByIndex(i);
      }
    } else if (condType == EStartStopConditionType.start) {
      for (var i = 0; i < (operation as DelayedOperation).startConditions().length; i++) {
        (operation as DelayedOperation).startConditions()[i].extLogicConditionId = new Utils().getLetterByIndex(i);
      }
    } else if (condType == EStartStopConditionType.stop) {
      for (var i = 0; i < (operation as DelayedOperation).stopConditions().length; i++) {
        (operation as DelayedOperation).stopConditions()[i].extLogicConditionId = new Utils().getLetterByIndex(i);
      }
    }

  }

  activateOperation(operation: Operation, active: boolean) {
    operation.active = active;
    this.changed = true;
  }

  deleteOperation(operation: Operation) {
    var index: number;
    for (index = 0; index < this.operations.length; index++) {
      if (operation.id == this.operations[index].id) {
        //this.operations.splice(index, 1);
        operation.deleted = true;
        this.changed = true;
        break;
      }
    }
  }

  undeleteOperation(operation: Operation) {
    var index: number;
    for (index = 0; index < this.operations.length; index++) {
      if (operation.id == this.operations[index].id) {
        operation.deleted = false;
        break;
      }
    }
  }

  cancelChanges() {
    this.changed = false;
    this.operations = new Array<Operation>();
    for (var op of this.operationsOrigin) {
      // this.operations.push(Object.assign(Object.create(op), op));
      this.operations.push(_.cloneDeep(op));
    }
  }

  saveOperations() {
    var ops: Object = new Object();
    ops['Operations'] = new Array<Object>();
    var tmpOperations = new Array<Operation>();

    for (var op of this.operations) {
      if (!op.deleted) {
        ops['Operations'].push(op.toJson());
        // tmpOperations.push(Object.assign(Object.create(op), op));
        tmpOperations.push(_.cloneDeep(op));
      }
    }

    console.log('>>> new operations: ', ops);

    this.restService.saveJson('operation_list', ops).subscribe(data => {
      //data = this.convertFromBufferToJson(data);
      console.log('data: ', data);
      console.log('>>> err code: ', data.errorCode);
      if(data.errorCode){

        this.flashMessage.show('Saving operations list failed: ' + data.result, {
          cssClass: 'custom-danger-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      } else {
        this.operations = tmpOperations;
        this.operationsOrigin = tmpOperations;
        this.changed = false;
        this.flashMessage.show('Operations saved successfully', {
          cssClass: 'custom-success-alert',  showCloseBtn:true, closeOnClick:true,
          timeout: 5000});
      }
    });
  }



  saveOperation(operation: Operation) {
    for (var index = 0; index < this.operations.length; index++) {
      if (this.operations[index].id == operation.id) {
        this.operations.splice(index, 1);
        break;
      }
    }
    this.operations.push(operation);
    this.saveOperations();
  }

  cloneOperation(operationId: number) : Operation{
    var tmp: Operation = this.getOperationById(operationId);
    // this.tmpOperation = Object.assign(Object.create(tmp),tmp);
    this.tmpOperation = _.cloneDeep(tmp);
    return this.tmpOperation;
  }


  cancelEditOperation() {
    this.tmpOperation = null;
  }

  createOperation(type: string) {
    if (type == 'regular') {
      this.tmpOperation = this.builder.createOperation({'active':"1", 'name':"", 'type':"normal", 'conditions':[], 'actions':[], 'conditionsExtLogic':''}, this.deviceManager);
    } else if (type = 'delayed') {
      this.tmpOperation = this.builder.createOperation({'active':"1", 'name':"", 'type':"delayed", 'delay':"", 'conditions_start':[], 'conditions_stop':[], 'actions':[], 'conditionsExtLogic':''}, this.deviceManager);
    }
  }
}
