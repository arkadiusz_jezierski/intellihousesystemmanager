import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map';


@Injectable()
export class ToolsService {

  constructor(private http: HttpClient) { }

  getImage() {
    let req = {
    }

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/getImage', req, {headers: headers});
  }

  getLatestLogs(latestLogId : string, filter : string) {
    let req = {
      "logId" : latestLogId,
      "filter" : filter
    }

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/getLatestLogs', req, {headers: headers});
  }

  getOlderLogs(oldestLogId : string, filter : string) {
    let req = {
      "logId" : oldestLogId,
      "filter" : filter
    }

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/getOlderLogs', req, {headers: headers});
  }

  getLogsQnty() {
    let req = {}

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/getLogsQnty', req, {headers: headers});
  }

  createUpdateActionWidgetsGroup(key : number, name : string) {
    let req = {
      "key" : key,
      "name" : name
    }

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/upsertActionWidgetsGroup', req, {headers: headers});
  }

  getActionWidgetsGroups() {
    let req = {}

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/getActionWidgetsGroups', req, {headers: headers});
  }

  getActionWidgetsGroup(key : number) {
    let req = {
      "key" : key
    }

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/getActionWidgetsGroup', req, {headers: headers});
  }

  deleteActionWidgetsGroup(key : number) {
    let req = {
      "key" : key
    }

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/deleteActionWidgetsGroup', req, {headers: headers});
  }

  createUpdateActionWidget(key : number, groupKey : number, type : string, active : boolean, name : string, details : string) {
    let req = {
      "key" : key,
      "groupKey" : groupKey,
      "type" : type,
      "active" : active
    }

    if (details && details.length > 0) {
      req['details'] = details;
    }

    if (name && name.length > 0) {
      req['name'] = name;
    }

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/upsertActionWidget', req, {headers: headers});
  }

  getActionWidgetsForGroup(groupKey : number) {
    let req = {"groupKey" : groupKey}

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/getActionWidgets', req, {headers: headers});
  }

  getActionWidget(key : number) {
    let req = {
      "key" : key
    }

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/getActionWidget', req, {headers: headers});
  }

  deleteActionWidget(key : number) {
    let req = {
      "key" : key
    }

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/deleteActionWidget', req, {headers: headers});
  }

  createUpdateWidgetAction(key : number, widgetKey : number, widgetEvent : string, 
      devCategory : string, devAddress : string, devCommand : string, devCommandValue : number) {
    let req = {
      "key" : key,
      "widgetKey" : widgetKey,
      "widgetEvent" : widgetEvent,
      "devCategory" : devCategory,
      "devAddress" : devAddress,
      "devCommand" : devCommand,
      "devCommandValue" : devCommandValue
    }

    if (devCommand && devCommand.length > 0) {
      req['devCommand'] = devCommand;
    }

    if (devCommandValue != undefined) {
      req['devCommandValue'] = devCommandValue;
    }

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/upsertWidgetAction', req, {headers: headers});
  }

  deleteWidgetAction(key : number) {
    let req = {
      "key" : key
    }

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    return this.http.post('/tools/deleteWidgetAction', req, {headers: headers});
  }
}
