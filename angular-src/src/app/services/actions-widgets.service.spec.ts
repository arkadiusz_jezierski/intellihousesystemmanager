import { TestBed } from '@angular/core/testing';

import { ActionsWidgetsService } from './actions-widgets.service';

describe('ActionsWidgetsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActionsWidgetsService = TestBed.get(ActionsWidgetsService);
    expect(service).toBeTruthy();
  });
});
