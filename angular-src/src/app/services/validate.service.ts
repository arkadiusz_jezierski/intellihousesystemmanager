import { Injectable } from '@angular/core';
import {
  Operation,
  Condition,
  Action,
  DeviceAction,
  GsmModemAction,
  DelayedOperation,
  TimeCondition,
  DeviceCondition,
  RestClientAction
} from '../models/operation';


@Injectable()
export class ValidateService {

  constructor() { }

  validateEmail(email){
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validatePhoneNumber(number){
    const re =  /^\+[0-9]{11}$/;
    return re.test(number);
  }

  validateExtendedConditionLogicPattern(logic){
    const re =  /^[A-Z()|&]+$/;
    return re.test(logic);
  }

  validateOperation(operation: Operation, result) {
    if (!operation.name || (operation.name && operation.name == '')) {
      result.error = 'Operation name cannot be empty';
      return false;
    }

    if (operation.active === undefined) {
      result.error = 'Missing operation attribute: active';
      return false;
    }

    if (!operation.type) {
      result.error = 'Missing operation attribute: type';
      return false;
    }

    if (!operation.actions || (operation.actions && operation.actions.length == 0)) {
      result.error = 'Operation must contains at least one action';
      return false;
    }

    if (operation.type == 'normal') {
      if (!operation.conditions || (operation.conditions && operation.conditions.length == 0)) {
        result.error = 'Operation must contains at least one condition';
        return false;
      }
    }



    if (operation.type == 'delayed') {
      if (!operation.conditions || (operation.conditions && (operation as DelayedOperation).startConditions().length == 0)) {
        result.error = 'Operation must contains at least one start condition';
        return false;
      }

      if (!operation.conditions || (operation.conditions && (operation as DelayedOperation).stopConditions().length == 0)) {
        result.error = 'Operation must contains at least one stop condition';
        return false;
      }

      if (!(operation as DelayedOperation).delay ||
        ((operation as DelayedOperation).delay && (operation as DelayedOperation).delay === "")) {
        result.error = 'Operation delay value cannot be empty';
        return false;
      }

      if (!(operation as DelayedOperation).delay ||
        ((operation as DelayedOperation).delay && isNaN(Number((operation as DelayedOperation).delay)))) {
        result.error = 'Operation delay parameter must be a number';
        return false;
      }

      if (operation.conditionsExtLogic && !this.validateExtendedLogic(operation.conditionsExtLogic, (operation as DelayedOperation).startConditions(), result)) {
        return false;
      }

      if ((operation as DelayedOperation).stopConditionsExtLogic && !this.validateExtendedLogic((operation as DelayedOperation).stopConditionsExtLogic, (operation as DelayedOperation).stopConditions(), result)) {
        return false;
      }
    } else {
      if (operation.conditionsExtLogic && !this.validateExtendedLogic(operation.conditionsExtLogic, operation.conditions, result)) {
        return false;
      }
    }

    return true;
  }

  validateExtendedLogic(logicText: String, conditions: Condition[], result) :boolean {
    if (!this.validateExtendedConditionLogicPattern(logicText)) {
      result.error = 'Invalid format of extended logic';
      return false;
    }

    let condIds: Array<String> = new Array<String>();
    for (let cond of conditions) {
      condIds.push(cond.extLogicConditionId);
    }

    let idPattern = /^[A-Z]$/;
    for (let index = 0; index < logicText.length; index++) {
      let sign = logicText.substring(index, index + 1);
      if (idPattern.test(sign) && condIds.indexOf(sign) < 0) {
        result.error = 'Extended logic contains invalid condition ID';
        return false;
      }
    }

    return true;
  }


  validateAction(action: Object, result) {

    if (!action || !action['type']) {
      result.error = 'Action type must be selected';
      return false;
    }

    if (action['type']) {
      if (action['type'] == 'gsmModem') {
        return this.validateGsmModemAction(action as GsmModemAction, result);
      }
      if (action['type'] == 'rest') {
        return this.validateRestClientAction(action as RestClientAction, result);
      }
      if (action['type'] == 'device') {
        if (this.validateDevice(action, result)) {
          return this.validateDeviceAction(action as DeviceAction, result);
        } else {
          return false;
        }
      }
    }

    return true;
  }

  validateGsmModemAction(action: GsmModemAction, result) {

    if (!action.number ||
      (action.number && !this.validatePhoneNumber(action.number))) {
      result.error = 'Phone number must have a valid format';
      return false;
    }

    if (!action.message) {
      result.error = 'Message must be provided';
      return false;
    }

    return true;

  }

  validateRestClientAction(action: RestClientAction, result) {

    if (!action.endpoint || action.endpoint == '') {
      result.error = 'Endpoint cannot be empty';
      return false;
    }

    if (!action.message) {
      result.error = 'Message must be provided';
      return false;
    }

    return true;

  }

  validateDeviceAction(action: DeviceAction, result) {

    if (!action.param) {
      result.error = 'Device parameter must be selected';
      return false;
    }

    if (action.value === null || action.value === "") {
      result.error = 'Parameter value cannot be empty';
      return false;
    }

    if (isNaN(Number(action.value))) {
      result.error = 'Parameter value must be a number';
      return false;
    }

    return true;

  }

  validateCondition(condition: Object, result) {

    if (!condition || !condition['type']) {
      result.error = 'Condition type must be selected';
      return false;
    }

    if (condition['type']) {
      if (condition['type'] == 'time') {
        return this.validateTimeCondition(condition as TimeCondition, result);

      }
      if (condition['type'] == 'device') {
        if (this.validateDevice(condition, result)) {
          return this.validateDeviceCondition(condition as DeviceCondition, result);
        } else {
          return false;
        }
      }
    }

    return true;
  }
  //
  validateTimeCondition(condition: TimeCondition, result) {
    if (!condition.condition) {
      result.error = 'Condition value must be selected';
      return false;
    }

    if (condition.value === null || condition.value === "") {
      result.error = 'Time value cannot be empty';
      return false;
    }

    if (isNaN(Number(condition.value))) {
      result.error = 'Time value must be a number';
      return false;
    }

    return true;

  }
  //
  validateDeviceCondition(condition: DeviceCondition, result) {
    if (!condition.condition) {
      result.error = 'Condition value must be selected';
      return false;
    }

    if (!condition.param) {
      result.error = 'Device parameter must be selected';
      return false;
    }

    if (condition.value === null || condition.value === "") {
      result.error = 'Parameter value cannot be empty';
      return false;
    }

    if (isNaN(Number(condition.value))) {
      result.error = 'Parameter value must be a number';
      return false;
    }

    return true;

  }

  validateDevice(device: Object, result) {
    if (!device['category']) {
      result.error = 'Device category must be selected';
      return false;
    }

    if (!device['address']) {
      result.error = 'Device must be selected';
      return false;
    }

    return true;
  }

}
