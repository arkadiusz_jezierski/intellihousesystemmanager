import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Device } from '../models/device';

import 'rxjs/add/operator/map';

@Injectable()
export class RestService {

  constructor(private http: Http) { }

  setDeviceParameter(device: Device, param: string, val: number) {
    let req = {
      "devCat":device.category,
      "devAddr":device.address,
      "param":param,
      "val":val
    }

    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('/rest/setDeviceParameter', req, {headers: headers})
      .map(res => res.json());
  }

  getDeviceParameter(device: Device, param: string) {
    let req = {
      "devCat":device.category,
      "devAddr":device.address,
      "param":param
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/getDeviceParameter', req, {headers: headers})
      .map(res => res.json());
  }

  getJson(jsonId) {
    let req = {
      "jsonId":jsonId
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/getJson', req, {headers: headers})
      .map(res => res.json());
  }

  saveJson(jsonId, jsonBody) {
    let req = {
      "jsonId":jsonId,
      "jsonBody":jsonBody
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/saveJson', req, {headers: headers})
      .map(res => res.json());
  }

  setDeviceName(device: Device) {
    let req = {
      "devCat":device.category,
      "devAddr":device.address,
      "name":device.name
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/setDeviceName', req, {headers: headers})
      .map(res => res.json());
  }

  getDevicesList() {
    let req = {}

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/getDevicesList', req, {headers: headers})
      .map(res => res.json());
  }

  getFilesList() {
    let req = {}

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/getFilesList', req, {headers: headers})
      .map(res => res.json());
  }

  uploadFirmware(fileName) {
    let req = {
      "fileName":fileName
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/uploadFirmware', req, {headers: headers})
      .map(res => res.json());
  }

  bootDevice(uid) {
    let req = {
      "uid":uid
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/bootDevice', req, {headers: headers})
      .map(res => res.json());
  }

  initDevices() {
    let req = {}

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/initDevices', req, {headers: headers})
      .map(res => res.json());
  }

  removeDevice(uid) {
    let req = {
      "uid":uid
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/removeDevice', req, {headers: headers})
      .map(res => res.json());
  }

  removeDeviceAdvance(cat, addrStart, addrStop) {
    let req = {
      "cat":cat,
      "addrStart":addrStart,
      "addrStop":addrStop
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/removeDeviceAdvance', req, {headers: headers})
      .map(res => res.json());
  }

  getDeviceDetails(device: Device) {
    let req = {
      "devCat":device.category,
      "devAddr":device.address
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/getDeviceDetails', req, {headers: headers})
      .map(res => res.json());
  }

  getDeviceParameters(device: Device) {
    console.log('>>> cat: ' + device.category);
    let req = {
      "devCat":device.category,
      "devAddr":device.address
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/getDeviceParameters', req, {headers: headers})
      .map(res => res.json());
  }

  getCategories() {
    let req = {}

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/getCategories', req, {headers: headers})
      .map(res => res.json());
  }

  sendMessage(number, message) {
    let req = {
      "number":number,
      "message":message
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/sendMessage', req, {headers: headers})
      .map(res => res.json());
  }

  armSystem(armDisarm, hash) {
    let req = {
      "armDisarm":armDisarm,
      "hash":hash
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/armSystem', req, {headers: headers})
      .map(res => res.json());
  }

  getArmSystemStatus() {
    let req = {}

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/rest/getArmSystemStatus', req, {headers: headers})
      .map(res => res.json());
  }

}
