import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class BackupService {

  constructor(private http: HttpClient) { }

  getData(name) {

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    var path = '/backup/' + name;
    return this.http.get(path, {headers: headers});
  }

  saveData(name, data) {

    let headers = new HttpHeaders();

    headers.append('Content-Type','application/json');
    var path = '/restore/' + name;
    return this.http.post(path, data, {headers: headers});
  }
}
