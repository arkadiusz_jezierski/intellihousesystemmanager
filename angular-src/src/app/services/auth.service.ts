import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import * as moment from 'moment';
import { StorageFactory } from '../utils/storageFactory';

@Injectable()
export class AuthService {

  authToken: any;
  user: any;
  expires_at: any;
  storage: StorageFactory;

  constructor(
    private http: Http
  ) {
    this.storage = new StorageFactory(localStorage);
  }

  authenticateUser(user) {
  console.log('>>>> auth user: ', user);
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('/users/authenticate', user, {headers: headers})
      .map(res => res.json());
  }

  storeUserData(token, user, expiresIn) {
    const expiresAt = moment().add(expiresIn,'second');
    this.storage.setItem('id_token', token);
    this.storage.setItem('user', JSON.stringify(user));
    this.storage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
    this.authToken = token;
    this.user = user;
    this.expires_at = expiresAt;
  }

  logout() {
    this.authToken = null;
    this.user = null;
    this.expires_at = null;
    this.storage.clear();
  }

  loadToken() {

    const token = this.storage.getItem('id_token');
    this.authToken = token;
  }

  getExpiration() {
    const expiration = this.storage.getItem("expires_at");
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }

  loggedIn() {
    return moment().isBefore(this.getExpiration());
    // return true;
  }
}
