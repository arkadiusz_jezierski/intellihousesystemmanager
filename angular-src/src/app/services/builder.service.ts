import { Injectable } from '@angular/core';
import { EStartStopConditionType } from '../models/consts';
import {
  Operation,
  RegularOperation,
  DelayedOperation,
  Action,
  DeviceAction,
  GsmModemAction,
  Condition,
  DeviceCondition,
  TimeCondition,
  AlarmCondition, RestClientAction
} from '../models/operation';
import { DeviceManagerService } from '../services/device-manager.service';

@Injectable()
export class BuilderService {

  constructor() { }

  createOperation(jsonObject: Object, devManager: DeviceManagerService) :Operation {
    var op : Operation;
    if (jsonObject['type'] == 'normal') {
      op = new RegularOperation(jsonObject);
      var jsonConditions = jsonObject['conditions'];
      if (jsonConditions) {
        op.conditions = new Array<Condition>();
        for (var cond of jsonConditions) {
          op.conditions.push(this.createCondition(cond, devManager, EStartStopConditionType.regular));
        }
      }

      var jsonActions = jsonObject['actions'];
      if (jsonActions) {
        op.actions = new Array<Action>();
        for (var act of jsonActions) {
          op.actions.push(this.createAction(act, devManager));
        }
      }
    } else if (jsonObject['type'] == 'delayed') {
      op = new DelayedOperation(jsonObject);
      op.conditions = new Array<Condition>();
      var jsonConditions = jsonObject['conditions_start'];
      if (jsonConditions) {
        for (var cond of jsonConditions) {
          op.conditions.push(this.createCondition(cond, devManager, EStartStopConditionType.start));
        }
      }

      var jsonConditions = jsonObject['conditions_stop'];
      if (jsonConditions) {
        for (var cond of jsonConditions) {
          op.conditions.push(this.createCondition(cond, devManager, EStartStopConditionType.stop));
        }
      }

      var jsonActions = jsonObject['actions'];
      if (jsonActions) {
        op.actions = new Array<Action>();
        for (var act of jsonActions) {
          op.actions.push(this.createAction(act, devManager));
        }
      }
    }

    return op;
  }

  createAction(jsonObject: Object, devManager: DeviceManagerService) :Action {
    if (jsonObject['type'] == 'device') {
      return new DeviceAction(jsonObject, devManager);
    } else if (jsonObject['type'] == 'gsmModem') {
      return new GsmModemAction(jsonObject, devManager);
    } else if (jsonObject['type'] == 'rest') {
      return new RestClientAction(jsonObject, devManager);
    }
  }

  createCondition(jsonObject: Object, devManager: DeviceManagerService, type: EStartStopConditionType) :Condition {
    if (jsonObject['type'] == 'device') {
      return new DeviceCondition(jsonObject, devManager, type);
    } else if (jsonObject['type'] == 'time') {
      return new TimeCondition(jsonObject, type);
    } else if (jsonObject['type'] == 'alarm') {
      return new AlarmCondition(jsonObject, type);
    } else {
      return null;
    }
  }

}
