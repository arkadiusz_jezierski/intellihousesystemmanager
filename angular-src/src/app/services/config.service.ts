import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Device } from '../models/device';

import 'rxjs/add/operator/map';

@Injectable()
export class ConfigService {

  constructor(private http: Http) { }

  addDevice(device: Device, key: string) {
    let req = {
      "key":key,
      "category":device.category,
      "address":device.address
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/config/addDevice', req, {headers: headers})
      .map(res => res.json());
  }

  getDevice(key: string) {
    let req = {
      "key":key
    }

    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('/config/getDevice', req, {headers: headers})
      .map(res => res.json());
  }

  getDevices(keys: string[]) {
    let req = {
      "keys":keys
    }

    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('/config/getDevices', req, {headers: headers})
      .map(res => res.json());
  }

  removeDevices() {
    let req;
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('/config/delDevices', req, {headers: headers})
      .map(res => res.json());
  }

  addBasicConfig(key: string, value: string) {
    let req = {
      "key":key,
      "value":value
    }

    let headers = new Headers();

    headers.append('Content-Type','application/json');
    return this.http.post('/config/addConfig', req, {headers: headers})
      .map(res => res.json());
  }

  getBasicConfig(key: string) {
    let req = {
      "key":key
    }

    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('/config/getConfig', req, {headers: headers})
      .map(res => res.json());
  }

  removeBasicConfigs() {
    let req;
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('/config/delConfig', req, {headers: headers})
      .map(res => res.json());
  }

  addDeviceConfig(key: string, device: Device, parameter: string, value: number) {
    let req = {
      "key":key,
      "value":value,
      "address":device.address,
      "category":device.category,
      "parameter":parameter
    }

    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('/config/addDeviceConfig', req, {headers: headers})
      .map(res => res.json());
  }

  getDevicesConfigs(key: string) {
    let req = {
      "key":key
    }

    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('/config/getDevicesConfig', req, {headers: headers})
      .map(res => res.json());
  }

  removeDevicesConfigs() {
    let req;
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post('/config/delDevicesConfig', req, {headers: headers})
      .map(res => res.json());
  }
}
