import {DeviceManagerService} from "../services/device-manager.service";

export class WidgetAction {
  key: number;
  widgetKey: number;
  widgetEvent: string;
  devCategory : string;
  devAddress : string;
  devCommand : string;
  devCommandValue : number;

  // constructor(
  //   key: number, widgetKey: number, widgetEvent: string, devCategory : string, devAddress : string,
  //   devCommand : string, devCommandValue : number
  // ) {
  //   this.key = key;
  //   this.widgetKey = widgetKey;
  //   this.widgetEvent = widgetEvent;
  //   this.devCategory = devCategory;
  //   this.devAddress = devAddress;
  //   this.devCommand = devCommand;
  //   this.devCommandValue = devCommandValue;
  // }

  constructor() {
  }

  getName(deviceManager : DeviceManagerService) : string {
    var devName = 'Uknown device';
    if (this.devCategory && this.devAddress) {
      var device = deviceManager.getDevice(this.devCategory, this.devAddress);
      devName = device.name;
    }

    var output = devName;

    if (this.devCommand != undefined) {
      output += ' set ' + this.devCommand;
    }

    if (this.devCommandValue != undefined) {
      output +=  ' to ' + this.devCommandValue;
    }

    return output;
  }

}
