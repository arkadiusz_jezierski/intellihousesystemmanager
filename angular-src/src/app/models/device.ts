import { Category } from './category';

export class Device {
  category: string;
  address: string;
  active: boolean;
  name: string;
  uid: number;
  lastSeen: Date;
  lastStartup: Date;
  resetCounter: number;
  wdtCounter: number;

  parameters: {};
  commands: {};

  constructor(jsonObject) {
    this.address = jsonObject['address'].toString();
    this.category = jsonObject['category'].toString();
    this.uid = jsonObject['uid'];
    this.active = jsonObject['active'];
    this.name = jsonObject['name'];
  }

  //greet() {
  //  return "Hello, " + this.greeting;
  //}
}
