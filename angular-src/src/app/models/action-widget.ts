import { WidgetAction } from "./widget-action";

export class ActionWidget {
  key: number;
  groupKey: number;
  type : string;
  active : boolean;
  details : string;
  name : string;
  actions : WidgetAction[];
  
  constructor(key : number, groupKey : number, type : string, active : boolean, name: string, details : string) {
    this.key = key;
    this.groupKey = groupKey;
    this.type = type;
    this.active = active;
    this.details = details;
    this.name = name;
  }

}
