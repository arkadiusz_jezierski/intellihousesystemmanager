import { ActionWidget } from "./action-widget";

export class ActionGroup {
  key: number;
  name: string;
  widgets : ActionWidget[];
  
  constructor(key : number, name : string) {
    this.key = key;
    this.name = name;
  }

}
