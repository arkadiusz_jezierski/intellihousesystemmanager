import { EParamConditionType, EStartStopConditionType } from './consts';
import { Device } from './device';
import { DeviceManagerService } from '../services/device-manager.service';
import { Utils } from '../utils/utils';

//  interfaces
export interface Operation {
  id: number;
  active: boolean;
  deleted: boolean;
  name: string;
  type: string;
  conditionsExtLogic: string;

  actions: Action[];
  conditions: Condition[];

  toJson(): Object;
}

export interface Action {
  type: string;
  id: number;

  toJson(): Object;
  toString(): string;
}

export interface Condition {

  type: string;
  internalType: EStartStopConditionType;
  value: string;
  id: number;
  extLogicConditionId: string;

  toJson(): Object;
  toString(): string;
}
//###################### interfaces


// operations
export class RegularOperation implements Operation{
  id: number;
  active: boolean;
  deleted: boolean;

  name: string;
  type: string;
  conditionsExtLogic: string;

  actions: Action[];
  conditions: Condition[];

  constructor(jsonOBject: Object) {
    this.active = (jsonOBject['active'] == '1');
    this.deleted = false;
    this.name = jsonOBject['name'];
    this.type = jsonOBject['type'];
    this.conditionsExtLogic = jsonOBject['extCondLogic'];

    this.id = new Utils().generateRandom();

  }

  toJson(): Object {
    var json: Object = new Object();

    var cons: Array<Object> = new Array<Object>();
    for (var con of this.conditions) {
      cons.push(con.toJson());
    }

    var acts: Array<Object> = new Array<Object>();
    for (var act of this.actions) {
      acts.push(act.toJson());
    }

    json['name'] = this.name;
    json['active'] = this.active ? '1' : '0';
    json['type'] = this.type;
    json['conditions'] = cons;
    json['actions'] = acts;
    json['extCondLogic'] = this.conditionsExtLogic;

    return json;
  }
}

export class DelayedOperation implements Operation {
  id: number;
  active: boolean;
  deleted: boolean;
  name: string;
  type: string;
  conditionsExtLogic: string;
  stopConditionsExtLogic: string;

  actions: Action[];
  conditions: Condition[];
  delay: string;

  constructor(jsonOBject: Object) {
    this.active = (jsonOBject['active'] == '1');
    this.deleted = false;
    this.name = jsonOBject['name'];
    this.type = jsonOBject['type'];
    this.delay = jsonOBject['delay'];
    this.conditionsExtLogic = jsonOBject['extCondLogic'];
    this.stopConditionsExtLogic = jsonOBject['stopExtCondLogic'];
    this.id = new Utils().generateRandom();
  }

  toJson(): Object {
    var json: Object = new Object();

    var stopCons: Array<Object> = new Array<Object>();
    for (var con of this.stopConditions()) {
      stopCons.push(con.toJson());
    }

    var startCons: Array<Object> = new Array<Object>();
    for (var con of this.startConditions()) {
      startCons.push(con.toJson());
    }

    var acts: Array<Object> = new Array<Object>();
    for (var act of this.actions) {
      acts.push(act.toJson());
    }

    json['name'] = this.name;
    json['active'] = this.active ? '1' : '0';
    json['type'] = this.type;
    json['delay'] = this.delay;
    json['conditions_stop'] = stopCons;
    json['conditions_start'] = startCons;
    json['actions'] = acts;
    json['extCondLogic'] = this.conditionsExtLogic;
    json['stopExtCondLogic'] = this.stopConditionsExtLogic;

    return json;
  }

  startConditions(): Condition[] {
    var cons: Condition[] = new Array<Condition>();
    for (var con of this.conditions) {
      if (con.internalType == EStartStopConditionType.start) {
        cons.push(con);
      }
    }
    return cons;
  }

  stopConditions(): Condition[] {
    var cons: Condition[] = new Array<Condition>();
    for (var con of this.conditions) {
      if (con.internalType == EStartStopConditionType.stop) {
        cons.push(con);
      }
    }
    return cons;
  }
}

// ####################### operations

// conditions

export class DeviceCondition implements Condition{
  constructor(jsonObject: Object, deviceManager: DeviceManagerService, internalType: EStartStopConditionType) {
    this.type = jsonObject['type'];
    this.internalType = internalType;
    this.param = jsonObject['param'];
    this.condition = jsonObject['condition'];
    this.value = jsonObject['value'].toString();
    this.device = deviceManager.getDevice(jsonObject['category'], jsonObject['address']);
    this.id = new Utils().generateRandom();
    this.extLogicConditionId = jsonObject['extId'];
  }

  id: number;
  type: string;
  internalType: EStartStopConditionType;
  device: Device;
  param: string;
  condition: EParamConditionType;
  value: string;
  extLogicConditionId: string;

  toJson(): Object {
    var json: Object = new Object();
    json['type'] = this.type;
    json['category'] = this.device.category.toString();
    json['address'] = this.device.address.toString();
    json['param'] = this.param;
    json['condition'] = this.condition;
    json['value'] = this.value;
    json['extId'] = this.extLogicConditionId

    return json;
  }

  toString(): string {
    return this.extLogicConditionId + ': ' + this.device.name + ' has ' + this.param + ' ' + this.condition + ' ' + this.value;
  }
}

  export class TimeCondition implements Condition{
  constructor(jsonObject: Object, internalType: EStartStopConditionType) {
    this.type = jsonObject['type'];
    this.internalType = internalType;
    this.condition = jsonObject['condition'];
    this.value = jsonObject['value'].toString();
    this.id = new Utils().generateRandom();
    this.extLogicConditionId = jsonObject['extId'];
  }

  id: number;
  type: string;
  internalType: EStartStopConditionType;
  condition: EParamConditionType;
  value: string;
  extLogicConditionId: string;

  toJson(): Object {
    var json: Object = new Object();
    json['type'] = this.type;
    json['condition'] = this.condition;
    json['value'] = this.value;
    json['extId'] = this.extLogicConditionId;
    return json;
  }

  toString(): string {
    console.log('>>> time: ', this.value);
    console.log('>>> time: ', new Utils().convertTime(this.value));
    return this.extLogicConditionId + ': Time ' + this.condition + ' ' + new Utils().convertTime(this.value);
  }
}

export class AlarmCondition implements Condition{
  constructor(jsonObject: Object, internalType: EStartStopConditionType) {
    this.type = jsonObject['type'];
    this.internalType = internalType;
    this.value = jsonObject['value'];
    this.id = new Utils().generateRandom();
    this.extLogicConditionId = jsonObject['extId'];
  }

  id: number;
  type: string;
  internalType: EStartStopConditionType;
  value: string;
  extLogicConditionId: string;

  toJson(): Object {
    var json: Object = new Object();
    json['type'] = this.type;
    json['value'] = this.value;
    json['extId'] = this.extLogicConditionId;
    return json;
  }

  toString(): string {
    var ret = this.extLogicConditionId + ': Alarm is ';
    console.log('>> ret: ' + ret);
    ret += (this.value == '1') ? 'active' : 'inactive';
    console.log('>>>> alarm to string: ' + ret);
    return ret;
  }
}

// ######################### conditions

// actions

export class DeviceAction implements Action{
  constructor(jsonObject: Object, deviceManager: DeviceManagerService) {
    this.type = jsonObject['type'];
    this.param = jsonObject['param'];
    this.value = jsonObject['value'].toString();
    this.device = deviceManager.getDevice(jsonObject['category'], jsonObject['address']);
    this.id = new Utils().generateRandom();
  }

  id: number;
  type: string;
  device: Device;
  param: string;
  value: string;

  toJson(): Object {
    var json: Object = new Object();
    json['type'] = this.type;
    json['category'] = this.device.category.toString();
    json['address'] = this.device.address.toString();
    json['param'] = this.param;
    json['value'] = this.value;
    return json;
  }

  toString(): string {
    return this.device.name + ' set ' + this.param + ' = ' + this.value;
  }
}

export class GsmModemAction implements Action{
  constructor(jsonObject: Object, deviceManager: DeviceManagerService) {
    this.id = new Utils().generateRandom();
    this.type = jsonObject['type'];
    this.message = jsonObject['message'];
    this.number = jsonObject['number'];
  }

  id: number;
  type: string;
  message: string;
  number: string;


  toJson(): Object {
    var json: Object = new Object();
    json['type'] = this.type;
    json['message'] = this.message;
    json['number'] = this.number;

    return json;
  }

  toString(): string {
    return 'Send SMS [' + this.message.substring(0, 20) + '] to ' + this.number;
  }
}

export class RestClientAction implements Action{
  constructor(jsonObject: Object, deviceManager: DeviceManagerService) {
    this.id = new Utils().generateRandom();
    this.type = jsonObject['type'];
    this.message = jsonObject['message'];
    this.endpoint = jsonObject['endpoint'];
  }

  id: number;
  type: string;
  message: string;
  endpoint: string;


  toJson(): Object {
    var json: Object = new Object();
    json['type'] = this.type;
    json['message'] = this.message;
    json['endpoint'] = this.endpoint;

    return json;
  }

  toString(): string {
    return 'Rest request [' + this.message.substring(0, 20) + '] to ' + this.endpoint;
  }
}

//############################# actions
