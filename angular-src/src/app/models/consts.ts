export enum EParamConditionType{
  undefined = -1,
  equal = 0,
  greater = 1,
  greater_equal,
  less,
  less_equal,
  any,
  rising,
  falling,
  not_equal
};

export enum EStartStopConditionType{
  regular,
  start,
  stop
};

export const SRC_PAGE_EVENT_MANAGER = 'event-manager';
export const SRC_PAGE_COND_EDIT = 'condition';
export const SRC_PAGE_ACT_EDIT = 'action';

export const URL_PARAM_ID = 'id';
export const URL_PARAM_SRC = 'src';
export const URL_PARAM_KEY = 'key';
export const URL_PARAM_EDIT = 'edit';

export const ALARM_ARMED = 'ALARM WŁĄCZONY';
export const ALARM_DISARMED = 'ALARM WYŁĄCZONY';
export const ALARM_ARMING = 'UZBRAJANIE...';

export const WIDGET_TYPE_BUTTONS_1 = '1-button';
export const WIDGET_TYPE_BUTTONS_2 = '2-buttons';
export const WIDGET_TYPE_BUTTONS_3 = '3-buttons';
export const WIDGET_TYPE_BUTTONS_4 = '4-buttons';
export const WIDGET_TYPE_SLIDER = 'slider';
export const WIDGET_TYPE_RGB_SLIDER = 'rgb-slider';
export const WIDGET_TYPE_RGB_PICKER = 'rgb-picker';

export const WIDGET_TYPES = [
  WIDGET_TYPE_BUTTONS_1,
  WIDGET_TYPE_BUTTONS_2,
  WIDGET_TYPE_BUTTONS_3,
  WIDGET_TYPE_BUTTONS_4,
  WIDGET_TYPE_SLIDER,
  WIDGET_TYPE_RGB_SLIDER,
  WIDGET_TYPE_RGB_PICKER
];

export const WIDGET_EVENT_RADIUS_CENTER = 'radius-center';
export const WIDGET_EVENT_RGB = 'color-picker-rgb';
export const WIDGET_EVENT_SLIDER = 'slider';
export const WIDGET_EVENT_BUTTON_1 = 'button-1';
export const WIDGET_EVENT_BUTTON_2 = 'button-2';
export const WIDGET_EVENT_BUTTON_3 = 'button-3';
export const WIDGET_EVENT_BUTTON_4 = 'button-4';

export const WIDGET_DETAILS_SLIDER_MAX = 'slider-max';
export const WIDGET_DETAILS_SHOW_SLIDER_GRAPHIC = 'slider-graphic';
export const WIDGET_DETAILS_SLIDER_GRAPHIC_COLOR = 'slider-graphic-color';
export const WIDGET_DETAILS_BUTTON_NAME = 'buttonName';
export const WIDGET_DETAILS_BUTTON_COLOR = 'buttonColor';
export const WIDGET_DETAILS_BUTTON_SETTINGS = 'buttonSettings';


//export enum EConditionType{
//  device,
//  time,
//  alarm,
//  COND_TYPE_DEVICE
//};


//get
