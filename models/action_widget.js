const mongoose = require('mongoose');
const config = require('../config/database');
const WidgetAction = require('../models/widget_action');


// ActionWidget Schema
const ActionWidgetSchema = mongoose.Schema({
    groupKey: {
        type: Number,
        required: true
    },
    key: {
        type: Number,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        required: true
    },
    details: {
        type: String,
        required: false
    },
    name: {
        type: String,
        required: false
    }
});

const ActionWidget = module.exports = mongoose.model('ActionWidget', ActionWidgetSchema);

module.exports.addUpdateActionWidget = function(widget, callback){
    //console.log('>>> newGroup: ', newGroup);
    //console.log('>>> newGroup key: ', newGroup.key);
    const query = {key: widget.key};
    const update = {
        groupKey: widget.groupKey,
        key: widget.key,
        type: widget.type,
        active: widget.active
    };

    if (widget.details != undefined) {
        update.details = widget.details;
    }
    if (widget.name != undefined) {
        update.name = widget.name;
    }

    const options  = { upsert: true };
    console.log('>>> query: ', query);
    console.log('>>> update: ', update);
    ActionWidget.findOneAndUpdate(query, update, options, function(error, result) {
        callback(error);
    });
}

module.exports.getActionWidgets = function(parentGroupKey, callback){
    const query = { groupKey: parentGroupKey };
    ActionWidget.find(query, callback);
}

module.exports.getAllActionWidgets = function(callback){
    const query = {};
    ActionWidget.find(query, callback);
}

module.exports.insertActionWidgets = function(widgets, callback){
    ActionWidget.collection.insert(widgets, callback);
}

module.exports.getActionWidget = function(widgetKey, callback){
    const query = { key: widgetKey };
    ActionWidget.find(query, callback);
}

module.exports.deleteActionWidget = function(widgetKey, callback){
    const query = { key : widgetKey };
    WidgetAction.deleteWidgetActions(widgetKey, function(error, result) {
        if (!error) {
            ActionWidget.remove(query, callback);
        } else {
            callback(error);
        }

    });
}

module.exports.deleteActionWidgets = function(groupKey, callback){
    const query = { groupKey : groupKey };
    ActionWidget.find(query, function(error, result) {
        if (!error) {
            var widgetIds = [];
            for (var widget of result) {
                widgetIds.push(widget.key);
            }
            WidgetAction.deleteWidgetsActions(widgetIds, function(error, result) {
                if (!error) {
                    ActionWidget.remove(query, callback);
                } else {
                    callback(error);
                }
            });
        } else {
            callback(error);
        }

    });

}

// module.exports.getActionWidgetsGroupByName = function(groupName, callback){
//     const query = {name: groupName}
//     ActionWidgetsGroup.findOne(query, callback);
// }

// module.exports.getDevicesByKey = function(keyNames, callback){
//     Device.find({key: { $in: keyNames }}).exec(callback);
// }

// module.exports.removeDevices = function(callback){
//     Device.remove({}, callback);
// }