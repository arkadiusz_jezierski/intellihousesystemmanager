const mongoose = require('mongoose');
const config = require('../config/database');


// Device Schema
const DeviceSchema = mongoose.Schema({
    key: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    }
});

const Device = module.exports = mongoose.model('Device', DeviceSchema);

module.exports.addDevice = function(newDevice, callback){
    const query = {key: newDevice.key};
    const update = { address: newDevice.address, category: newDevice.category };
    const options  = { upsert: true };
    Device.findOneAndUpdate(query, update, options, function(error, result) {
        callback(error);
    });
}

module.exports.getDeviceByKey = function(keyName, callback){
    const query = {key: keyName}
    Device.findOne(query, callback);
}

module.exports.getDevicesByKey = function(keyNames, callback){
    Device.find({key: { $in: keyNames }}).exec(callback);
}

module.exports.removeDevices = function(callback){
    Device.remove({}, callback);
}

module.exports.getAllDevices = function(callback){
    Device.find({}, callback);
}

module.exports.insertAllDevices = function(devices, callback){
    Device.collection.insert(devices, callback);
}