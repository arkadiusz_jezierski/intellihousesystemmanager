const mongoose = require('mongoose');
const config = require('../config/database');


// BasicConfig Schema
const BasicConfigSchema = mongoose.Schema({
    key: {
        type: String,
        required: true
    },
    value: {
        type: String,
        required: true
    }
});

const BasicConfig = module.exports = mongoose.model('BasicConfig', BasicConfigSchema);

module.exports.setConfig = function(config, callback){
    const query = {key: config.key};
    const update = { value: config.value };
    const options  = { upsert: true };
    BasicConfig.findOneAndUpdate(query, update, options, function(error, result) {
        callback(error);
    });
}

module.exports.getConfig = function(keyName, callback){
    const query = {key: keyName}
    BasicConfig.findOne(query, callback);
}

module.exports.removeConfigs = function(callback){
    BasicConfig.remove({}, callback);
}

module.exports.getAllBasicConfigs = function(callback){
    BasicConfig.find({}, callback);
}

module.exports.insertAllBasicConfigs = function(configs, callback){
    BasicConfig.collection.insert(configs, callback);
}