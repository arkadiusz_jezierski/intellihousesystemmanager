const mongoose = require('mongoose');
const config = require('../config/database');


// WidgetAction Schema
const WidgetActionSchema = mongoose.Schema({
    widgetKey: {
        type: Number,
        required: true
    },
    key: {
        type: Number,
        required: true
    },
    widgetEvent: {
        type: String,
        required: true
    },
    devCategory: {
        type: String,
        required: true
    },
    devAddress: {
        type: String,
        required: true
    },
    devCommand: {
        type: String,
        required: false
    },
    devCommandValue: {
        type: Number,
        required: false
    }
});

const WidgetAction = module.exports = mongoose.model('WidgetAction', WidgetActionSchema);

module.exports.addUpdateWidgetAction = function(action, callback){
    //console.log('>>> newGroup: ', newGroup);
    //console.log('>>> newGroup key: ', newGroup.key);
    const query = {key: action.key};
    const update = {
        widgetKey: action.widgetKey,
        key: action.key,
        widgetEvent: action.widgetEvent,
        devCategory: action.devCategory,
        devAddress: action.devAddress
    };

    if (action.devCommandValue != undefined) {
        update.devCommandValue = action.devCommandValue;
    }

    if (action.devCommand != undefined) {
        update.devCommand = action.devCommand;
    }

    const options  = { upsert: true };
    console.log('>>> query: ', query);
    console.log('>>> update: ', update);
    WidgetAction.findOneAndUpdate(query, update, options, function(error, result) {
        callback(error);
    });
}

module.exports.insertWidgetActions = function(actions, callback){
    WidgetAction.collection.insert(actions, callback);
}

module.exports.getWidgetActions = function(parentWidgetKey, callback){
    const query = { widgetKey: parentWidgetKey };
    WidgetAction.find(query, callback);
}

module.exports.getAllWidgetActions = function(callback){
    const query = { };
    WidgetAction.find(query, callback);
}

module.exports.getWidgetsActions = function(parentWidgetKeys, callback){
    const query = { widgetKey : { "$in" : parentWidgetKeys }};
    WidgetAction.find(query, callback);
}

module.exports.deleteWidgetAction = function(actionKey, callback){
    const query = { key : actionKey };
    WidgetAction.remove(query, callback);
}

module.exports.deleteWidgetActions = function(parentWidgetKey, callback){
    const query = { widgetKey : parentWidgetKey };
    WidgetAction.remove(query, callback);
}

module.exports.deleteWidgetsActions = function(parentWidgetKeys, callback){
    const query = { widgetKey : { "$in" : parentWidgetKeys }};
    WidgetAction.remove(query, callback);
}

// module.exports.getActionWidgetsGroupByName = function(groupName, callback){
//     const query = {name: groupName}
//     ActionWidgetsGroup.findOne(query, callback);
// }

// module.exports.getDevicesByKey = function(keyNames, callback){
//     Device.find({key: { $in: keyNames }}).exec(callback);
// }

// module.exports.removeDevices = function(callback){
//     Device.remove({}, callback);
// }